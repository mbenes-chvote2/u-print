/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.privatecredentials

import ch.ge.ve.javafx.business.json.JsonPathMapper
import ch.ge.ve.javafx.business.progress.ProgressTracker
import ch.ge.ve.protocol.core.algorithm.ChannelSecurityAlgorithms
import ch.ge.ve.protocol.model.PublicParameters
import ch.ge.ve.uprint.business.ArchiveHelper
import ch.ge.ve.uprint.business.ContextHelper
import ch.ge.ve.uprint.business.CryptoTestConfig
import ch.ge.ve.uprint.business.TestResources
import ch.ge.ve.uprint.business.context.factory.ControlComponentsPublicKeysFileFactory
import ch.ge.ve.uprint.business.context.factory.PrinterArchiveFactory
import ch.ge.ve.uprint.business.context.factory.PrivateKeyFactory
import ch.ge.ve.uprint.business.solr.EmbeddedSolrService
import ch.ge.ve.uprint.business.solr.api.SolrService
import ch.ge.ve.uprint.business.solr.api.UprintSolrCore
import ch.ge.ve.uprint.business.solr.factory.SolrServerFactory
import ch.ge.ve.uprint.business.solr.model.PrivateCredentialsDocument
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.solr.client.solrj.SolrQuery
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

@ContextConfiguration(classes = [CryptoTestConfig])
class PrivateCredentialsDeserializerTest extends Specification {

  @Rule
  TemporaryFolder temporaryFolder = new TemporaryFolder()

  @Autowired
  PublicParameters publicParameters

  @Autowired
  ObjectMapper objectMapper

  @Autowired
  JsonPathMapper jsonPathMapper

  @Autowired
  ChannelSecurityAlgorithms channelSecurityAlgorithms

  ContextHelper contextHelper
  SolrService solrService

  def setup() {
    solrService = new EmbeddedSolrService(new SolrServerFactory().getObject())
    contextHelper = new ContextHelper(
            new ControlComponentsPublicKeysFileFactory(publicParameters, jsonPathMapper),
            new PrinterArchiveFactory(),
            new PrivateKeyFactory(publicParameters),
            new ArchiveHelper(temporaryFolder)
    )
  }

  def cleanup() {
    solrService.deleteAll()
    solrService.close()
  }

  def "should import private credentials"() {
    given:
    def solrClient = solrService.getClient()
    def context = contextHelper.createContext(
            TestResources.PRINTER_ARCHIVE_VALID_STANDARD_BALLOT,
            temporaryFolder.newFolder("printer-file").toPath(),
            Mock(ProgressTracker)
    )
    def deserializer = new PrivateCredentialsDeserializer(
            solrClient, objectMapper, channelSecurityAlgorithms, Mock(ProgressTracker), context
    )

    when:
    def importReport = deserializer.doImport()

    then:
    def response = solrClient.query(UprintSolrCore.PRIVATE_CREDENTIALS.getCoreName(), new SolrQuery("*:*"))
    response.results.getNumFound() == 22
    importReport.getNbrOfDocuments() == 22
    def result = response.getBeans(PrivateCredentialsDocument.class)
    result.get(0).getVoterId() == "WzHRUjNBFv_pe-ak9zwknMn7uFde2GFkXMc0"
    result.get(0).getCcIndex() == 0
    result.get(0).getPid() == "0_WzHRUjNBFv_pe-ak9zwknMn7uFde2GFkXMc0"
    result.get(0).getPrivateCredentials().startsWith("{\"voter\"")
  }
}
