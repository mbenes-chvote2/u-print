/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.gui.controller;

import ch.ge.ve.javafx.gui.utils.FileBrowserService;
import ch.ge.ve.uprint.business.context.api.ContextService;
import ch.ge.ve.uprint.business.context.factory.ControlComponentsPublicKeysFileFactory;
import ch.ge.ve.uprint.business.context.model.Context;
import ch.ge.ve.uprint.business.context.model.ControlComponentsPublicKeysFile;
import com.jfoenix.controls.JFXTextField;
import java.nio.file.Path;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * JavaFX Controller for the import of the control components' public keys.
 */
@Component
public class ImportPublicKeysController {

  private final FileBrowserService                     fileBrowserService;
  private final ContextService                         contextService;
  private final ControlComponentsPublicKeysFileFactory controlComponentsPublicKeysFileFactory;
  private final ApplicationState                       applicationState;

  private Path publicKeysFile;

  @FXML
  private JFXTextField publicKeysPath;

  @FXML
  private VBox summaryBox;

  @FXML
  private Label keysNotImportedLabel;

  @FXML
  private Label availableKeysLabel;

  @Autowired
  public ImportPublicKeysController(FileBrowserService fileBrowserService, ContextService contextService,
                                    ControlComponentsPublicKeysFileFactory controlComponentsPublicKeysFileFactory,
                                    ApplicationState applicationState) {
    this.fileBrowserService = fileBrowserService;
    this.contextService = contextService;
    this.controlComponentsPublicKeysFileFactory = controlComponentsPublicKeysFileFactory;
    this.applicationState = applicationState;
  }

  /**
   * If a set of control components public keys have already been imported a box with a summary containing the file
   * location and its digest will be displayed.
   */
  @FXML
  public void initialize() {
    upateSummary();
  }

  private void upateSummary() {
    Context context = contextService.get();
    summaryBox.getChildren().clear();
    ControlComponentsPublicKeysFile controlComponentsPublicKeys = context.getControlComponentsPublicKeysFile();
    if (controlComponentsPublicKeys != null) {
      summaryBox.getChildren().add(availableKeysLabel);
      summaryBox.getChildren().add(new Label(String.format("\t%s : %s",
                                                           controlComponentsPublicKeys.getPath(),
                                                           controlComponentsPublicKeys.getSha256sum())));
    } else {
      summaryBox.getChildren().add(keysNotImportedLabel);
    }
  }

  /**
   * Open the file chooser dialog.
   */
  @FXML
  public void browserFiles() {
    publicKeysFile = fileBrowserService.selectFile(publicKeysPath, "json");
  }

  /**
   * Validate the public key path and imports the control components' public keys.
   *
   * @see ch.ge.ve.uprint.gui.validator.ControlComponentsPublicKeysFileValidator
   */
  @FXML
  public void importFile() {
    publicKeysPath.resetValidation();
    if (publicKeysPath.validate()) {
      contextService.get().setControlComponentsPublicKeysFile(
          controlComponentsPublicKeysFileFactory.readControlComponentsPublicKeysFile(publicKeysFile));
      clearFilePath();
      upateSummary();
      applicationState.setControlComponentsPublicKeysImported(true);
    }
  }

  private void clearFilePath() {
    publicKeysFile = null;
    publicKeysPath.setText(null);
    publicKeysPath.requestLayout();
  }
}
