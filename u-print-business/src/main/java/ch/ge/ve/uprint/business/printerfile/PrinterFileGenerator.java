/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.printerfile;

import ch.ge.ve.chvote.pactback.contract.printerarchive.PrinterOperationConfigurationVo;
import ch.ge.ve.filenamer.FileNamer;
import ch.ge.ve.interfaces.ech.eCH0045.v4.VotingPersonType;
import ch.ge.ve.interfaces.ech.eCH0058.v5.HeaderType;
import ch.ge.ve.interfaces.ech.eCH0159.v4.Delivery;
import ch.ge.ve.interfaces.ech.eCH0228.v1.VotingCardDeliveryType;
import ch.ge.ve.javafx.business.json.JsonPathMapper;
import ch.ge.ve.javafx.business.progress.ProgressTracker;
import ch.ge.ve.javafx.business.xml.HeaderTypeFactory;
import ch.ge.ve.javafx.business.xml.VoterChoicesFactory;
import ch.ge.ve.model.convert.model.VoterChoice;
import ch.ge.ve.protocol.core.algorithm.VotingCardPreparationAlgorithms;
import ch.ge.ve.protocol.core.model.SecretVoterData;
import ch.ge.ve.protocol.core.model.VotingCard;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.uprint.business.context.model.PrinterArchive;
import ch.ge.ve.uprint.business.ech0228.ECH0228CodeDesignation;
import ch.ge.ve.uprint.business.ech0228.ECH0228Utils;
import ch.ge.ve.uprint.business.printerfile.exception.PrinterFileGenerationException;
import ch.ge.ve.uprint.business.solr.SolrBatchIterator;
import ch.ge.ve.uprint.business.solr.api.SolrService;
import ch.ge.ve.uprint.business.solr.api.UprintSolrCore;
import ch.ge.ve.uprint.business.solr.model.PrivateCredentialsDocument;
import ch.ge.ve.uprint.business.solr.model.VoterDocument;
import ch.ge.ve.uprint.business.xml.ECHUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.util.ClientUtils;
import org.codehaus.stax2.XMLInputFactory2;
import org.codehaus.stax2.XMLOutputFactory2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * An object to generate a printer file.
 */
@Service
public class PrinterFileGenerator {
  private final SolrService         solrService;
  private final ObjectMapper        objectMapper;
  private final JsonPathMapper      jsonPathMapper;
  private final VoterChoicesFactory voterChoicesFactory;
  private final HeaderTypeFactory   headerTypeFactory;
  private final boolean             useVoterIdAsSequenceNumber;
  private final JAXBContext         jaxbContext;

  private final XMLOutputFactory xmlOutputFactory = XMLOutputFactory2.newFactory();
  private final XMLInputFactory  xmlInputFactory  = XMLInputFactory2.newInstance();
  private final XMLEventFactory  eventFactory     = XMLEventFactory.newInstance();

  private int nextSequenceNumber;

  /**
   * Create a new instance of printer file generator.
   *
   * @param solrService                the {@link SolrService}.
   * @param objectMapper               the object mapper.
   * @param jsonPathMapper             the json mapper.
   * @param headerTypeFactory          an ECH header factory.
   * @param voterChoicesFactory        a {@link VoterChoice} factory.
   * @param initialSequenceNumber      the sequence number of the first voting card.
   * @param useVoterIdAsSequenceNumber whether the sequence number is generated or the voter id is used.
   */
  @Autowired
  public PrinterFileGenerator(SolrService solrService,
                              ObjectMapper objectMapper,
                              JsonPathMapper jsonPathMapper,
                              HeaderTypeFactory headerTypeFactory,
                              VoterChoicesFactory voterChoicesFactory,
                              @Value("${uprint.initialSequenceNumber}") Integer initialSequenceNumber,
                              @Value("${uprint.useVoterIdAsSequenceNumber:false}") boolean useVoterIdAsSequenceNumber) {
    this.solrService = solrService;
    this.objectMapper = objectMapper;
    this.jsonPathMapper = jsonPathMapper;
    this.headerTypeFactory = headerTypeFactory;
    this.voterChoicesFactory = voterChoicesFactory;
    this.nextSequenceNumber = initialSequenceNumber == null ? 0 : initialSequenceNumber;
    this.useVoterIdAsSequenceNumber = useVoterIdAsSequenceNumber;

    try {
      this.jaxbContext = JAXBContext.newInstance(VotingPersonType.class,
                                                 Delivery.class,
                                                 VotingCardDeliveryType.ContestData.class,
                                                 VotingCardDeliveryType.VotingCard.class);
    } catch (JAXBException e) {
      throw new PrinterFileGenerationException("Cannot initialize printer file generator", e);
    }
  }

  /**
   * Start the generation of the printer file.
   *
   * @param printerArchive  the printer archive source.
   * @param outputFolder    the output folder where the generated printer file will be written to.
   * @param progressTracker the progress tracking strategy.
   */
  public void generatePrinterFile(PrinterArchive printerArchive,
                                  Path outputFolder,
                                  ProgressTracker progressTracker) {

    final LocalDateTime now = LocalDateTime.now();
    final String filename = FileNamer.ech228Archive(
        printerArchive.getOperationName(), printerArchive.getPrinterName(), now);

    try (ZipOutputStream out = new ZipOutputStream(Files.newOutputStream(outputFolder.resolve(filename)))) {
      out.putNextEntry(
          new ZipEntry(FileNamer.ech228File(printerArchive.getOperationName(), printerArchive.getPrinterName(), now))
      );

      PublicParameters publicParameters =
          jsonPathMapper.map(printerArchive.getPublicParametersFile(), PublicParameters.class);
      ElectionSetWithPublicKey electionSet =
          jsonPathMapper.map(printerArchive.getElectionSetFile(), ElectionSetWithPublicKey.class);
      PrinterOperationConfigurationVo operationConfiguration =
          jsonPathMapper.map(printerArchive.getOperationConfigurationFile(), PrinterOperationConfigurationVo.class);

      List<VoterChoice> voterChoices = voterChoicesFactory.createVoterChoices(
          electionSet, printerArchive.getOperationReferenceFiles()
      );

      VotingCardPreparationAlgorithms votingCardPreparationAlgorithms =
          new VotingCardPreparationAlgorithms(publicParameters);

      XMLEventWriter writer = xmlOutputFactory.createXMLEventWriter(out, StandardCharsets.UTF_8.name());

      long nbrOfProcessedVoters = 0;
      long nbrOfVoters = retrieveNbrOfVoters();

      writeHeader(writer);
      writer.add(eventFactory.createStartElement(ECH0228Utils.ECH_0228_P, ECH0228Utils.ECH_0228_NS,
                                                 ECH0228Utils.VOTING_CARD_DELIVERY));
      writeContestData(writer, operationConfiguration);

      Iterator<VoterDocument> it = retrieveVotersIterator();
      while (it.hasNext()) {
        progressTracker.updateProgress(nbrOfProcessedVoters, nbrOfVoters);

        VoterDocument voter = it.next();
        VotingCard votingCard = retrieveVotingCard(voter, electionSet, votingCardPreparationAlgorithms);
        writeVotingCard(voter, votingCard, voterChoices, writer);

        nbrOfProcessedVoters = nbrOfProcessedVoters + 1;
      }

      writer.add(eventFactory.createEndElement(ECH0228Utils.ECH_0228_P, ECH0228Utils.ECH_0228_NS,
                                               ECH0228Utils.VOTING_CARD_DELIVERY));
      writer
          .add(eventFactory.createEndElement(ECH0228Utils.ECH_0228_P, ECH0228Utils.ECH_0228_NS, ECH0228Utils.DELIVERY));

      writer.add(eventFactory.createEndDocument());
      writer.flush();
      writer.close();

      progressTracker.updateProgress(nbrOfVoters, nbrOfVoters);

      out.closeEntry();

      copyOperationReferenceFiles(out, printerArchive.getOperationReferenceFiles());
    } catch (Exception e) {
      throw new PrinterFileGenerationException("An error occurred while writing printer file", e);
    }
  }

  private void copyOperationReferenceFiles(ZipOutputStream out, List<Path> operationReferenceFiles) {
    operationReferenceFiles.forEach(file -> {
      try {
        out.putNextEntry(new ZipEntry(file.getFileName().toString()));
        Files.copy(file, out);
        out.closeEntry();
      } catch (IOException e) {
        throw new PrinterFileGenerationException(
            String.format("An error occurred while copying operation reference file with name: [%s]",
                          file.getFileName()), e);
      }
    });
  }

  private void writeVotingCard(VoterDocument voter,
                               VotingCard votingCard,
                               List<VoterChoice> voterChoices,
                               XMLEventWriter writer) throws XMLStreamException, JAXBException {
    VotingCardDeliveryType.VotingCard votingCardDelivery = new VotingCardDeliveryType.VotingCard();

    VotingPersonType votingPersonType = retrieveVotingPerson(voter);
    votingCardDelivery.setVotingPerson(ECH0228Utils.mapECH0045VotingPersonType(votingPersonType));

    if (useVoterIdAsSequenceNumber) {
      votingCardDelivery.setVotingCardsequenceNumber(votingCard.getVoter().getVoterId());
    } else {
      votingCardDelivery.setVotingCardsequenceNumber(nextSequenceNumber++);
    }

    // TODO set votingPlaceInformation
    // TODO set votingCardReturnAddress

    votingCardDelivery.setEVotingIndividualCodes(
        VerificationCodesConverter.toEVotingIndividualCodes(votingCard, voterChoices)
    );
    votingCardDelivery.getIndividualLogisticCode().add(
        ECH0228CodeDesignation.CONFIRMATION.createNamedCodeType(votingCard.getUpper_y())
    );
    votingCardDelivery.getIndividualLogisticCode().add(
        ECH0228CodeDesignation.FINALIZATION.createNamedCodeType(votingCard.getUpper_fc())
    );

    writeObject(VotingCardDeliveryType.VotingCard.class, votingCardDelivery, ECH0228Utils.VOTING_CARD, writer);
  }

  private VotingPersonType retrieveVotingPerson(VoterDocument voter) throws XMLStreamException, JAXBException {
    Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

    XMLEventReader reader = xmlInputFactory.createXMLEventReader(
        new ByteArrayInputStream(voter.getVoterXmlElement().getBytes(StandardCharsets.UTF_8)));
    VotingPersonType votingPersonType = null;

    while (reader.hasNext() && votingPersonType == null) {
      XMLEvent event = reader.peek();

      if (event.isStartElement() && ECH0228Utils.VOTER.equals(event.asStartElement().getName().getLocalPart())) {
        votingPersonType = unmarshaller.unmarshal(reader, VotingPersonType.class).getValue();
      } else {
        reader.next();
      }
    }

    reader.close();

    return votingPersonType;
  }

  private void writeHeader(XMLEventWriter writer) throws XMLStreamException, JAXBException {
    writer.add(eventFactory.createStartDocument(StandardCharsets.UTF_8.name(), "1.0"));
    ECHUtils.setCommonPrefixes(writer);
    writer.setPrefix(ECH0228Utils.ECH_0228_P, ECH0228Utils.ECH_0228_NS);

    writer.add(eventFactory.createStartElement(ECH0228Utils.ECH_0228_P,
                                               ECH0228Utils.ECH_0228_NS,
                                               ECH0228Utils.DELIVERY));

    ECHUtils.writeCommonNamespaces(writer, eventFactory);
    writer.add(eventFactory.createNamespace(ECH0228Utils.ECH_0228_P, ECH0228Utils.ECH_0228_NS));

    writeObject(HeaderType.class, headerTypeFactory.createHeader(), ECH0228Utils.DELIVERY_HEADER, writer);
  }

  private void writeContestData(XMLEventWriter writer,
                                PrinterOperationConfigurationVo operationConfiguration) throws JAXBException {

    VotingCardDeliveryType.ContestData contestData = new VotingCardDeliveryType.ContestData();

    contestData.setContestIdentification(operationConfiguration.getOperationName());
    contestData.getEVotingContestCodes().add(
        ECH0228CodeDesignation.VOTING_CARD_TITLE.createNamedCodeType(operationConfiguration.getVotingCardLabel())
    );

    writeObject(VotingCardDeliveryType.ContestData.class, contestData, ECH0228Utils.CONTEST_DATA, writer);
  }

  private <T> void writeObject(Class<T> declaredType, T value, String elementName, XMLEventWriter writer)
      throws JAXBException {
    Marshaller marshaller = jaxbContext.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

    marshaller.marshal(
        new JAXBElement<>(new QName(ECH0228Utils.ECH_0228_NS, elementName), declaredType, value), writer
    );
  }

  private Long retrieveNbrOfVoters() {
    SolrQuery query = votersQuery();
    query.setRows(0);
    try {
      return solrService.getClient()
                        .query(UprintSolrCore.ELECTORAL_REGISTER.getCoreName(), query)
                        .getResults()
                        .getNumFound();
    } catch (IOException | SolrServerException e) {
      throw new PrinterFileGenerationException("Cannot retrieve the total number of voters", e);
    }
  }

  private SolrQuery votersQuery() {
    SolrQuery query = new SolrQuery();
    query.setQuery("*:*");
    return query;
  }

  private Iterator<VoterDocument> retrieveVotersIterator() {
    SolrQuery query = votersQuery();
    query.setSort("voterId", SolrQuery.ORDER.asc);
    return new SolrBatchIterator<>(solrService.getClient(),
                                   UprintSolrCore.ELECTORAL_REGISTER, query,
                                   VoterDocument.class);
  }

  private VotingCard retrieveVotingCard(VoterDocument voter,
                                        ElectionSetWithPublicKey electionSet,
                                        VotingCardPreparationAlgorithms votingCardPreparationAlgorithms) {
    List<List<SecretVoterData>> secretVoterDataList =
        retrievePrivateCredentials(voter.getVoterId())
            .stream().sorted(Comparator.comparing(PrivateCredentialsDocument::getCcIndex))
            .map(PrivateCredentialsDocument::getPrivateCredentials)
            .map(privateCredentials -> {
              try {
                return Collections.singletonList(objectMapper.readValue(privateCredentials, SecretVoterData.class));
              } catch (IOException e) {
                throw new PrinterFileGenerationException(
                    String.format("Cannot deserialize private credentials for voter with id: [%s]",
                                  voter.getVoterId()), e);
              }
            }).collect(Collectors.toList());

    return votingCardPreparationAlgorithms.getVotingCards(electionSet, secretVoterDataList).get(0);
  }

  private List<PrivateCredentialsDocument> retrievePrivateCredentials(String voterId) {
    SolrQuery query = new SolrQuery();
    query.setQuery("voterId:\"" + ClientUtils.escapeQueryChars(voterId) + "\"");
    try {
      return solrService.getClient().query(UprintSolrCore.PRIVATE_CREDENTIALS.getCoreName(), query)
                        .getBeans(PrivateCredentialsDocument.class);
    } catch (IOException | SolrServerException e) {
      throw new PrinterFileGenerationException(
          String.format("Cannot retrieve private credentials for voter with id: [%s]", voterId), e);
    }
  }
}
