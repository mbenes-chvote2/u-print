/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.context.exception;

import ch.ge.ve.javafx.business.exception.ErrorCodeBasedException;

/**
 * Thrown when an invalid public keys file is provided to the application.
 */
public class InvalidPublicKeysFileException extends ErrorCodeBasedException {

  private static final String ERROR_CODE_SUFFIX = "INVALID_PUBLIC_KEYS_FILE";

  /**
   * Create a new invalid public keys file exception.
   *
   * @param message the detail message.
   *
   * @see RuntimeException#RuntimeException(String, Throwable)
   */
  public InvalidPublicKeysFileException(String message) {
    super(ERROR_CODE_SUFFIX, message);
  }

  /**
   * Create a new invalid public keys file exception.
   *
   * @param message the detail message.
   * @param cause   the cause of this exception.
   *
   * @see RuntimeException#RuntimeException(String, Throwable)
   */
  public InvalidPublicKeysFileException(String message, Throwable cause) {
    super(ERROR_CODE_SUFFIX, message, cause);
  }
}
