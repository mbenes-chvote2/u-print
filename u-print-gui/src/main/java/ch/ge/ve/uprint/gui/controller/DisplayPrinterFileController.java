/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.gui.controller;

import ch.ge.ve.javafx.business.progress.ProgressTracker;
import ch.ge.ve.javafx.gui.control.MessageContainer;
import ch.ge.ve.javafx.gui.progress.ProgressBarTracker;
import ch.ge.ve.javafx.gui.utils.FXMLLoaderUtils;
import ch.ge.ve.javafx.gui.utils.FileBrowserService;
import ch.ge.ve.javafx.gui.utils.TaskUtils;
import ch.ge.ve.uprint.business.context.api.ContextService;
import ch.ge.ve.uprint.business.context.factory.PrinterFileFactory;
import ch.ge.ve.uprint.business.ech0228.provider.ECH0228ParserProvider;
import ch.ge.ve.uprint.business.solr.api.SolrService;
import ch.ge.ve.uprint.business.solr.api.UprintSolrCore;
import ch.ge.ve.uprint.business.solr.model.ImportReport;
import ch.ge.ve.uprint.gui.controller.exception.TaskExecutionException;
import com.google.common.base.Stopwatch;
import com.jfoenix.controls.JFXProgressBar;
import com.jfoenix.controls.JFXTextField;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

/**
 * JavaFX Controller for the printer file visualization.
 */
@Component
public class DisplayPrinterFileController {
  private static final String ERROR_TRANSLATION_KEY = "display-printer-file.exception";

  private static final Logger logger = LoggerFactory.getLogger(DisplayPrinterFileController.class);

  private final ApplicationContext    applicationContext;
  private final FileBrowserService    fileBrowserService;
  private final ContextService        contextService;
  private final ECH0228ParserProvider ech0228ParserProvider;
  private final SolrService           solrService;
  private final TaskExecutor          taskExecutor;
  private final PrinterFileFactory    printerFileFactory;
  private final ApplicationState      applicationState;

  private CountDownLatch isContextReadySignal;

  @FXML
  private MessageContainer errorMessageContainer;

  @FXML
  private VBox votingCardBox;

  @FXML
  private JFXTextField printerFilePath;

  @FXML
  private HBox extractPrinterFileProgressRow;

  @FXML
  private JFXProgressBar extractPrinterFileProgress;

  @FXML
  private HBox importPrinterFileProgressRow;

  @FXML
  private JFXProgressBar importPrinterFileProgress;

  @Autowired
  public DisplayPrinterFileController(ApplicationContext applicationContext,
                                      FileBrowserService fileBrowserService,
                                      ContextService contextService,
                                      ECH0228ParserProvider ech0228ParserProvider,
                                      SolrService solrService,
                                      TaskExecutor taskExecutor,
                                      PrinterFileFactory printerFileFactory,
                                      ApplicationState applicationState) {
    this.applicationContext = applicationContext;
    this.fileBrowserService = fileBrowserService;
    this.contextService = contextService;
    this.ech0228ParserProvider = ech0228ParserProvider;
    this.solrService = solrService;
    this.taskExecutor = taskExecutor;
    this.printerFileFactory = printerFileFactory;
    this.applicationState = applicationState;
  }

  /**
   * If there are currently imported printer files, loads the {@link VotingCardsController} view.
   */
  @FXML
  public void initialize() {
    if (contextService.get().getPrinterFile() != null) {
      loadVotingCardView();
    }
  }

  /**
   * Open the file chooser dialog to import the printer's file.
   */
  @FXML
  public void selectPrinterFile() {
    fileBrowserService.selectFile(printerFilePath, "zip");
  }

  /**
   * Validate the printer file path and starts the voting card import. All previously imported voting cards will be
   * deleted before performing new import.
   */
  @FXML
  public void doImport() {
    if (printerFilePath.validate()) {
      TaskUtils.clearAllIcons(extractPrinterFileProgressRow, importPrinterFileProgressRow);

      applicationState.setPrinterFileImportInProgress(true);

      votingCardBox.setManaged(false);
      votingCardBox.setVisible(false);
      errorMessageContainer.setMessage(null);

      isContextReadySignal = new CountDownLatch(1);

      prepareContext();
      doImportPrinterFile();
    }
  }

  private void prepareContext() {
    Task<Duration> task = new Task<>() {
      @Override
      protected Duration call() {
        Stopwatch stopwatch = Stopwatch.createStarted();
        ProgressTracker tracker = new ProgressBarTracker(extractPrinterFileProgress);

        contextService.get().setPrinterFile(null);
        solrService.delete(UprintSolrCore.VOTING_CARDS);

        contextService.get().setPrinterFile(
            printerFileFactory.readPrinterFile(Paths.get(printerFilePath.getText()), tracker)
        );

        return stopwatch.stop().elapsed();
      }
    };

    task.setOnFailed(event -> {
      logger.error("Failed to prepare the context", event.getSource().getException());
      onPrepareContextFailed(task);
    });

    task.setOnSucceeded(event -> {
      try {
        TaskUtils.onSucceeded(extractPrinterFileProgressRow);
        logger.info("Printer file preparation took [{}] seconds", task.get().getSeconds());
      } catch (ExecutionException e) {
        logger.error("Cannot retrieve the result of the prepare context task", e);
        onPrepareContextFailed(task);
        throw new TaskExecutionException("Cannot retrieve the result of the prepare context task", e);
      } catch (InterruptedException e) {
        logger.error("The prepare context task was interrupted", e);
        onPrepareContextFailed(task);
        Thread.currentThread().interrupt();
      } finally {
        isContextReadySignal.countDown();
      }
    });

    taskExecutor.execute(task);
  }

  private <T> void onPrepareContextFailed(Task<T> task) {
    onTaskFailed(task, extractPrinterFileProgressRow, extractPrinterFileProgress);
    contextService.get().setPrinterFile(null);
    isContextReadySignal.countDown();
  }

  private void doImportPrinterFile() {
    Task<ImportReport> task = new Task<>() {
      @Override
      protected ImportReport call() throws Exception {
        isContextReadySignal.await();
        ProgressTracker progressTracker = new ProgressBarTracker(importPrinterFileProgress);
        return ech0228ParserProvider.get(progressTracker).doImport();
      }
    };

    task.setOnFailed(event -> {
      logger.error("Failed to import the printer file", event.getSource().getException());
      onImportPrinterFileFailed(task);
    });

    task.setOnSucceeded(event -> {
      try {
        TaskUtils.onSucceeded(importPrinterFileProgressRow);

        loadVotingCardView();

        logger.info("Printer file import took [{}] seconds", task.get().getElapsedTime().getSeconds());
      } catch (ExecutionException e) {
        logger.error("Cannot retrieve the result of the import printer file task", e);
        onImportPrinterFileFailed(task);
        throw new TaskExecutionException("Cannot retrieve the result of the import printer file task", e);
      } catch (InterruptedException e) {
        logger.error("The Import printer file task was interrupted", e);
        onImportPrinterFileFailed(task);
        Thread.currentThread().interrupt();
      } finally {
        applicationState.setPrinterFileImportInProgress(false);
      }
    });

    taskExecutor.execute(task);
  }

  private <T> void onImportPrinterFileFailed(Task<T> task) {
    onTaskFailed(task, importPrinterFileProgressRow, importPrinterFileProgress);
    contextService.get().setPrinterFile(null);
    applicationState.setPrinterFileImportInProgress(false);
  }

  private <T> void onTaskFailed(Task<T> task, Pane progressBarContainer, JFXProgressBar progressBar) {
    TaskUtils.onFailed(task, progressBarContainer);
    errorMessageContainer.setMessage(TaskUtils.formatErrorMessage(ERROR_TRANSLATION_KEY, task));
    Platform.runLater(() -> progressBar.setProgress(1));
  }

  private void loadVotingCardView() {
    votingCardBox.setManaged(true);
    votingCardBox.setVisible(true);

    votingCardBox.getChildren().clear();
    votingCardBox.getChildren().add(FXMLLoaderUtils.load(applicationContext, "/view/VotingCards.fxml"));
  }

  /*--------------------------------------------------------------------------
   - Properties                                                              -
   ---------------------------------------------------------------------------*/

  /**
   * Whether a printer file is being imported for visualization.
   *
   * @return true if the import task is running, false otherwise.
   *
   * @see ApplicationState#printerFileImportInProgressProperty()
   */
  public boolean isPrinterFileImportInProgress() {
    return applicationState.printerFileImportInProgressProperty().get();
  }

  /**
   * Whether a printer file is being imported for visualization.
   *
   * @return a boolean {@link javafx.beans.property.ReadOnlyProperty} that describes whether a printer file is being
   * imported for visualization.
   *
   * @see ApplicationState#printerFileImportInProgressProperty()
   */
  public ReadOnlyBooleanProperty printerFileImportInProgressProperty() {
    return applicationState.printerFileImportInProgressProperty();
  }
}
