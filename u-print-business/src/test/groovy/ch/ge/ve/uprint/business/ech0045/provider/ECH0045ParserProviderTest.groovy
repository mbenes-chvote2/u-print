/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.ech0045.provider

import ch.ge.ve.javafx.business.progress.ProgressTracker
import ch.ge.ve.uprint.business.context.api.ContextService
import ch.ge.ve.uprint.business.context.model.Context
import ch.ge.ve.uprint.business.context.model.PrinterArchive
import ch.ge.ve.uprint.business.solr.api.SolrService
import java.nio.file.Paths
import org.apache.solr.client.solrj.SolrClient
import spock.lang.Specification

class ECH0045ParserProviderTest extends Specification {
  ECH0045ParserProvider ech0045ParserProvider
  SolrService solrService
  ContextService contextService

  def setup() {
    solrService = Mock(SolrService)
    contextService = Mock(ContextService)
    ech0045ParserProvider = new ECH0045ParserProvider(solrService, contextService)
  }

  def "should provide a new instance of ECH0045Parser on every call"() {
    given:
    def tracker = Mock(ProgressTracker)
    def context = Mock(Context)
    def printerArchive = Mock(PrinterArchive)
    printerArchive.ech0045 >> Paths.get("ech0045.xml")
    context.printerArchive >> printerArchive
    contextService.get() >> context
    solrService.getClient() >> Mock(SolrClient)

    when:
    def parser = ech0045ParserProvider.get(tracker)

    then:
    !parser.is(ech0045ParserProvider.get(tracker))
  }

}
