/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.solr;

import ch.ge.ve.uprint.business.solr.api.UprintSolrCore;
import ch.ge.ve.uprint.business.solr.exception.SolrRuntimeException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;

/**
 * A strategy to submit documents to a solr index by batches. The batch size can be determined in the constructor,
 * otherwise the value will be 1000 by default.
 * <p>
 * Call {@link #flush()} to force the commit.
 * </p>
 *
 * @param <T> the type of solr document
 */
public class SolrBatchInserter<T> {

  private static final int DEFAULT_BATCH_SIZE = 1000;

  private final SolrClient     solrClient;
  private final UprintSolrCore core;
  private final int            batchSize;
  private final List<T>        documents;

  private long nbrOfDocumentsImported = 0;

  /**
   * Create a new solr batch inserter with a default batch size of 1000 documents.
   *
   * @param solrClient the solr client.
   * @param core       the core where this batch inserter will import documents.
   */
  public SolrBatchInserter(SolrClient solrClient, UprintSolrCore core) {
    this(solrClient, core, DEFAULT_BATCH_SIZE);
  }

  /**
   * Create a new solr batch inserter with the given parameters.
   *
   * @param solrClient the solr client.
   * @param core       the core where this batch inserter will import documents.
   * @param batchSize  the batch size.
   */
  public SolrBatchInserter(SolrClient solrClient, UprintSolrCore core, int batchSize) {
    this.solrClient = solrClient;
    this.core = core;
    this.batchSize = batchSize;
    this.documents = new ArrayList<>();
  }

  /**
   * Add the given document, flush amd commit all changes if the specified batch size has been reached.
   *
   * @param document the document to be inserted.
   */
  public void add(T document) {
    documents.add(document);

    if (documents.size() >= batchSize) {
      flush();
    }
  }

  /**
   * Flush the batch regardless of its current size and commit the changes.
   *
   * @return the number of elements that were flushed.
   */
  public int flush() {
    int size = documents.size();

    if (size > 0) {
      try {
        solrClient.addBeans(core.getCoreName(), documents);
        solrClient.commit(core.getCoreName());
      } catch (IOException | SolrServerException e) {
        throw new SolrRuntimeException("A problem occurred when trying to flush the current batch", e);
      }

      documents.clear();
      nbrOfDocumentsImported += size;
    }

    return size;
  }

  /**
   * Return the total number of comitted documents by this instance.
   *
   * @return the number of imported documents.
   */
  public long getNbrOfDocumentsImported() {
    return nbrOfDocumentsImported;
  }
}
