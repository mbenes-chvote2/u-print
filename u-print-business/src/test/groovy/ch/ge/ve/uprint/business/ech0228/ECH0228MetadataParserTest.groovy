/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.ech0228

import ch.ge.ve.uprint.business.TestResources
import spock.lang.Specification

class ECH0228MetadataParserTest extends Specification {

  def "should parse operation configuration"() {
    given:
    def printerFile = TestResources.ECH0228
    def parser = new ECH0228MetadataParser()

    when:
    def operationConfiguration = parser.parse(printerFile)

    then:
    operationConfiguration.getOperationName() == "201901VP"
    operationConfiguration.getVotingCardLabel() == "Test cards"
  }
}