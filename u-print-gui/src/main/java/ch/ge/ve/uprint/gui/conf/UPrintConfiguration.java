/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.gui.conf;

import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Deserializer;
import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Serializer;
import ch.ge.ve.model.convert.api.VoterChoiceConverter;
import ch.ge.ve.model.convert.impl.DefaultVoterChoiceConverter;
import ch.ge.ve.protocol.core.algorithm.ChannelSecurityAlgorithms;
import ch.ge.ve.protocol.core.algorithm.KeyEstablishmentAlgorithms;
import ch.ge.ve.protocol.core.exception.ChannelSecurityException;
import ch.ge.ve.protocol.core.model.AlgorithmsSpec;
import ch.ge.ve.protocol.core.support.Hash;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.support.PublicParametersFactory;
import ch.ge.ve.uprint.business.solr.factory.SolrServerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import java.math.BigInteger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * Spring Configuration class for the U-PRINT application.
 */
@Configuration
@ComponentScan("ch.ge.ve")
public class UPrintConfiguration {

  @Bean
  public SolrServerFactory solrServerFactory() {
    return new SolrServerFactory();
  }

  @Bean
  public VoterChoiceConverter voterChoiceConverter() {
    return new DefaultVoterChoiceConverter();
  }

  @Bean
  public ObjectMapper objectMapper() {
    SimpleModule module = new SimpleModule();
    module.addSerializer(BigInteger.class, new BigIntegerAsBase64Serializer());
    module.addDeserializer(BigInteger.class, new BigIntegerAsBase64Deserializer());
    return new ObjectMapper().registerModule(module);
  }

  @Bean
  public TaskExecutor taskExecutor() {
    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(5);
    executor.setMaxPoolSize(10);
    executor.setQueueCapacity(25);
    return executor;
  }

  @Bean
  public PublicParameters publicParameters(@Value("${uprint.security-level}") int securityLevel,
                                           @Value("${uprint.nbrOfControlComponents}") int numberOfControlComponents) {
    PublicParametersFactory publicParametersFactory = PublicParametersFactory.forLevel(securityLevel);
    return publicParametersFactory.createPublicParameters(numberOfControlComponents);
  }

  @Bean
  public Hash hash(PublicParameters publicParameters,
                   @Value("${uprint.digest.algorithm}") String digestAlgorithm,
                   @Value("${uprint.digest.provider}") String digestProvider) {
    return new Hash(digestAlgorithm, digestProvider, publicParameters.getSecurityParameters().getUpper_l());
  }

  @Bean
  public RandomGenerator randomGenerator(@Value("${uprint.rng.algorithm}") String rngAlgorithm,
                                         @Value("${uprint.rng.provider}") String rngProvider) {
    return RandomGenerator.create(rngAlgorithm, rngProvider);
  }

  @Bean
  public ChannelSecurityAlgorithms channelSecurityAlgorithms(
      PublicParameters publicParameters, AlgorithmsSpec algorithms,
      Hash hash, RandomGenerator randomGenerator) throws ChannelSecurityException {
    return new ChannelSecurityAlgorithms(algorithms, hash, publicParameters.getIdentificationGroup(), randomGenerator);
  }

  @Bean
  public KeyEstablishmentAlgorithms keyEstablishmentAlgorithms(RandomGenerator randomGenerator) {
    return new KeyEstablishmentAlgorithms(randomGenerator);
  }

  @Bean
  public AlgorithmsSpec algorithms(@Value("${uprint.keyspec-algorithm}") String keySpecAlgorithm,
                                   @Value("${uprint.cipher.algorithm}") String cipherAlgorithm,
                                   @Value("${uprint.cipher.provider}") String cipherProvider,
                                   @Value("${uprint.digest.algorithm}") String digestAlgorithm,
                                   @Value("${uprint.digest.provider}") String digestProvider,
                                   @Value("${uprint.rng.algorithm}") String rngAlgorithm,
                                   @Value("${uprint.rng.provider}") String rngProvider) {
    return new AlgorithmsSpec(keySpecAlgorithm, cipherProvider, cipherAlgorithm,
                              digestProvider, digestAlgorithm, rngProvider, rngAlgorithm);
  }
}
