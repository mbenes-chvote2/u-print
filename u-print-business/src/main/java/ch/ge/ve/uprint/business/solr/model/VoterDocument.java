/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.solr.model;

import org.apache.solr.client.solrj.beans.Field;

/**
 * The data for a voter stored as a document in the Solr index. Used to store the voter into the Solr index as well as
 * to query voters from the Solr index.
 */
public class VoterDocument {

  @Field
  private String voterId;

  @Field
  private String voterXmlElement;

  /**
   * Create a new voter document with the given parameters.
   *
   * @param voterId         The unique identifier of a voter, that is, the value of a <personId> element of an eCH-045
   *                        XML file.
   * @param voterXmlElement The complete raw XML element of a voter, that is, a <voter> element in an eCH-045 XML file.
   *                        Includes all necessary namespace information.
   */
  public VoterDocument(String voterId, String voterXmlElement) {
    this.voterId = voterId;
    this.voterXmlElement = voterXmlElement;
  }

  /**
   * Create an empty voter document. Required by solr in order to create the object with Reflection.
   */
  public VoterDocument() {

  }

  public String getVoterId() {
    return voterId;
  }

  public void setVoterId(String voterId) {
    this.voterId = voterId;
  }

  public String getVoterXmlElement() {
    return voterXmlElement;
  }

  public void setVoterXmlElement(String voterXmlElement) {
    this.voterXmlElement = voterXmlElement;
  }
}
