/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.context.factory

import ch.ge.ve.javafx.business.json.JsonPathMapper
import ch.ge.ve.protocol.model.PublicParameters
import ch.ge.ve.uprint.business.CryptoTestConfig
import ch.ge.ve.uprint.business.TestResources
import ch.ge.ve.uprint.business.context.exception.InvalidPublicKeysFileException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

@ContextConfiguration(classes = [CryptoTestConfig])
class ControlComponentsPublicKeysFileFactoryTest extends Specification {

  @Autowired
  PublicParameters publicParameters

  @Autowired
  JsonPathMapper jsonPathMapper

  ControlComponentsPublicKeysFileFactory controlComponentsPublicKeysFileFactory

  def setup() {
    controlComponentsPublicKeysFileFactory = new ControlComponentsPublicKeysFileFactory(publicParameters, jsonPathMapper)
  }

  def "should import valid control components public keys file"() {
    given:
    def source = TestResources.CONTROL_COMPONENT_KEYS

    when:
    def publicKeysFile = controlComponentsPublicKeysFileFactory.readControlComponentsPublicKeysFile(source)

    then:
    publicKeysFile.path == source
    publicKeysFile.sha256sum.equalsIgnoreCase("A880C00506B30CFEB659C8CEF5A6745D30889F0AA00C0D0C446B3CAFA227E260")
    publicKeysFile.publicKeys.size() == 2
    publicKeysFile.publicKeys[0].size() == 1
    publicKeysFile.publicKeys[0][0].id == "cc0.1-20180628"
    publicKeysFile.publicKeys[1].size() == 1
    publicKeysFile.publicKeys[1][0].id == "cc1.1-20180628"
  }

  def "should fail to import control components public keys file having an invalid control component name"() {
    given:
    def source = TestResources.CONTROL_COMPONENT_KEYS_WITH_INVALID_CC

    when:
    controlComponentsPublicKeysFileFactory.readControlComponentsPublicKeysFile(source)

    then:
    thrown(InvalidPublicKeysFileException)
  }

  def "should fail to import control components public keys file having more keys than expected"() {
    given:
    def source = TestResources.CONTROL_COMPONENT_KEYS_WITH_MORE_KEY_THAN_EXPECTED

    when:
    controlComponentsPublicKeysFileFactory.readControlComponentsPublicKeysFile(source)

    then:
    thrown(InvalidPublicKeysFileException)
  }

  def "should fail to import control components public keys file having an invalid structure"() {
    given:
    def source = TestResources.CONTROL_COMPONENT_KEYS_WITH_INVALID_STRUCTURE

    when:
    controlComponentsPublicKeysFileFactory.readControlComponentsPublicKeysFile(source)

    then:
    thrown(InvalidPublicKeysFileException)
  }
}
