/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.solr.model;

import org.apache.solr.client.solrj.beans.Field;

/**
 * The private credentials of a voter stored as a document in the Solr index. Used to store the credentials into the
 * Solr index as well as to query voters from the Solr index.
 */
public class PrivateCredentialsDocument {
  @Field
  private String voterId;

  @Field
  private Integer ccIndex;

  @Field
  private String pid;

  @Field
  private String privateCredentials;

  /**
   * Create a new private credentials document with the given parameters.
   *
   * @param voterId            The unique identifier of a voter.
   * @param ccIndex            the control component's index for this private credentials document.
   * @param privateCredentials The private credentials contained in the printer file.
   */
  public PrivateCredentialsDocument(String voterId, Integer ccIndex, String privateCredentials) {
    this.voterId = voterId;
    this.ccIndex = ccIndex;
    this.pid = ccIndex + "_" + voterId;
    this.privateCredentials = privateCredentials;
  }

  /**
   * Create an empty private credentials document. Required by solr in order to create the object with Reflection.
   */
  public PrivateCredentialsDocument() {

  }

  public String getVoterId() {
    return voterId;
  }

  public void setVoterId(String voterId) {
    this.voterId = voterId;
  }

  public Integer getCcIndex() {
    return ccIndex;
  }

  public void setCcIndex(Integer ccIndex) {
    this.ccIndex = ccIndex;
  }

  public String getPid() {
    return pid;
  }

  public String getPrivateCredentials() {
    return privateCredentials;
  }

  public void setPrivateCredentials(String privateCredentials) {
    this.privateCredentials = privateCredentials;
  }
}
