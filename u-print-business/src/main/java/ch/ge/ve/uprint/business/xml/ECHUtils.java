/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.xml;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;

/**
 * A utility class that contains common functionality for parsing and generating ECH files.
 */
public class ECHUtils {

  private static final String ECH_0007_NS = "http://www.ech.ch/xmlns/eCH-0007/6";
  private static final String ECH_0008_NS = "http://www.ech.ch/xmlns/eCH-0008/3";
  private static final String ECH_0010_NS = "http://www.ech.ch/xmlns/eCH-0010/6";
  private static final String ECH_0011_NS = "http://www.ech.ch/xmlns/eCH-0011/8";
  private static final String ECH_0021_NS = "http://www.ech.ch/xmlns/eCH-0021/7";
  private static final String ECH_0044_NS = "http://www.ech.ch/xmlns/eCH-0044/4";
  private static final String ECH_0045_NS = "http://www.ech.ch/xmlns/eCH-0045/4";
  private static final String ECH_0058_NS = "http://www.ech.ch/xmlns/eCH-0058/5";
  private static final String ECH_0155_NS = "http://www.ech.ch/xmlns/eCH-0155/4";
  private static final String ECH_0159_NS = "http://www.ech.ch/xmlns/eCH-0159/4";

  private static final String ECH_0007_P = "e7";
  private static final String ECH_0008_P = "e8";
  private static final String ECH_0010_P = "e10";
  private static final String ECH_0011_P = "e11";
  private static final String ECH_0021_P = "e21";
  private static final String ECH_0044_P = "e44";
  private static final String ECH_0045_P = "e45";
  private static final String ECH_0058_P = "e58";
  private static final String ECH_0155_P = "e155";
  private static final String ECH_0159_P = "e159";

  /**
   * Set ECH namespace prefixes to the given {@link XMLEventWriter}.
   *
   * @param eventWriter the {@link XMLEventWriter}.
   *
   * @return the eventWriter with the namespace attached.
   *
   * @throws XMLStreamException If there was an underlying stream issue.
   */
  public static XMLEventWriter setCommonPrefixes(XMLEventWriter eventWriter) throws XMLStreamException {
    eventWriter.setPrefix(ECH_0007_P, ECH_0007_NS);
    eventWriter.setPrefix(ECH_0008_P, ECH_0008_NS);
    eventWriter.setPrefix(ECH_0010_P, ECH_0010_NS);
    eventWriter.setPrefix(ECH_0011_P, ECH_0011_NS);
    eventWriter.setPrefix(ECH_0021_P, ECH_0021_NS);
    eventWriter.setPrefix(ECH_0044_P, ECH_0044_NS);
    eventWriter.setPrefix(ECH_0045_P, ECH_0045_NS);
    eventWriter.setPrefix(ECH_0058_P, ECH_0058_NS);
    eventWriter.setPrefix(ECH_0155_P, ECH_0155_NS);
    eventWriter.setPrefix(ECH_0159_P, ECH_0159_NS);

    return eventWriter;
  }

  /**
   * Attache namespace declaration. This method assumes that the writer is on a START_ELEMENT event.
   *
   * @param eventWriter  the {@link XMLEventWriter}.
   * @param eventFactory an instance of {@link XMLEventFactory}.
   *
   * @return the eventWriter with the namespace attached.
   *
   * @throws XMLStreamException If there was an underlying stream issue.
   */
  public static XMLEventWriter writeCommonNamespaces(XMLEventWriter eventWriter,
                                                     XMLEventFactory eventFactory) throws XMLStreamException {

    eventWriter.add(eventFactory.createNamespace(ECH_0007_P, ECH_0007_NS));
    eventWriter.add(eventFactory.createNamespace(ECH_0008_P, ECH_0008_NS));
    eventWriter.add(eventFactory.createNamespace(ECH_0010_P, ECH_0010_NS));
    eventWriter.add(eventFactory.createNamespace(ECH_0011_P, ECH_0011_NS));
    eventWriter.add(eventFactory.createNamespace(ECH_0021_P, ECH_0021_NS));
    eventWriter.add(eventFactory.createNamespace(ECH_0044_P, ECH_0044_NS));
    eventWriter.add(eventFactory.createNamespace(ECH_0045_P, ECH_0045_NS));
    eventWriter.add(eventFactory.createNamespace(ECH_0058_P, ECH_0058_NS));
    eventWriter.add(eventFactory.createNamespace(ECH_0155_P, ECH_0155_NS));
    eventWriter.add(eventFactory.createNamespace(ECH_0159_P, ECH_0159_NS));

    return eventWriter;
  }

  /**
   * Hide utility class constructor
   */
  private ECHUtils() {
    throw new AssertionError("Not instantiable");
  }
}
