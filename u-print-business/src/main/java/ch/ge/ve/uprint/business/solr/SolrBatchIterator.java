/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.solr;

import ch.ge.ve.uprint.business.solr.api.UprintSolrCore;
import ch.ge.ve.uprint.business.solr.exception.SolrRuntimeException;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;

/**
 * Iterate over all the documents returned by a given solr query.
 *
 * @param <T> the type of document.
 */
public class SolrBatchIterator<T> implements Iterator<T> {
  private static final int DEFAULT_BATCH_SIZE = 1000;

  private final SolrClient     solrClient;
  private final UprintSolrCore core;
  private final SolrQuery      query;
  private final Class<T>       type;
  private final int            batchSize;

  private Iterator<T> batchIterator;
  private int         start;

  /**
   * Create a new solr batch iterator with a default batch size of 1000 documents.
   *
   * @param solrClient the {@link SolrClient} that will execute the query.
   * @param core       the core where the documents will be requested.
   * @param query      the query to iterate.
   * @param type       the type of document to return.
   */
  public SolrBatchIterator(SolrClient solrClient, UprintSolrCore core, SolrQuery query, Class<T> type) {
    this(solrClient, core, query, type, DEFAULT_BATCH_SIZE);
  }

  /**
   * Create a new solr batch iterator with the given parameters.
   *
   * @param solrClient the {@link SolrClient} that will execute the query.
   * @param core       the core where the documents will be requested.
   * @param query      the query to iterate.
   * @param type       the type of document to return.
   * @param batchSize  the number of documents that will be retrieved by each batch.
   */
  public SolrBatchIterator(SolrClient solrClient, UprintSolrCore core, SolrQuery query, Class<T> type, int batchSize) {
    this.solrClient = solrClient;
    this.core = core;
    this.query = query;
    this.type = type;
    this.batchSize = batchSize;
    this.start = 0;
  }

  private void nextBatch() {
    try {
      query.setStart(start);
      query.setRows(batchSize);

      batchIterator = solrClient.query(core.getCoreName(), query).getBeans(type).iterator();
      start = start + batchSize;
    } catch (SolrServerException | IOException e) {
      throw new SolrRuntimeException("", e);
    }
  }

  @Override
  public boolean hasNext() {
    if (batchIterator == null || !batchIterator.hasNext()) {
      nextBatch();
    }

    return batchIterator.hasNext();
  }

  @Override
  public T next() {
    if (!hasNext()) {
      throw new NoSuchElementException();
    } else {
      return batchIterator.next();
    }
  }
}
