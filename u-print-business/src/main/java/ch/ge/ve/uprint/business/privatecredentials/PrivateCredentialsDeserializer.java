/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.privatecredentials;

import ch.ge.ve.filenamer.EpfFileName;
import ch.ge.ve.javafx.business.progress.ProgressTracker;
import ch.ge.ve.protocol.core.algorithm.ChannelSecurityAlgorithms;
import ch.ge.ve.protocol.core.exception.ChannelSecurityException;
import ch.ge.ve.protocol.core.model.SecretVoterData;
import ch.ge.ve.protocol.model.EncryptedMessageWithSignature;
import ch.ge.ve.uprint.business.context.model.Context;
import ch.ge.ve.uprint.business.context.model.ControlComponentPublicKey;
import ch.ge.ve.uprint.business.context.model.ControlComponentsPublicKeysFile;
import ch.ge.ve.uprint.business.privatecredentials.exception.CredentialsDeserializationException;
import ch.ge.ve.uprint.business.solr.DocumentImporter;
import ch.ge.ve.uprint.business.solr.api.UprintSolrCore;
import ch.ge.ve.uprint.business.solr.model.ImportReport;
import ch.ge.ve.uprint.business.solr.model.PrivateCredentialsDocument;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Stopwatch;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.apache.solr.client.solrj.SolrClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A specialized {@link DocumentImporter} to deserialize the private credentials of a printer archive and to store them
 * by voter id into a Solr index.
 */
public class PrivateCredentialsDeserializer extends DocumentImporter<PrivateCredentialsDocument> {
  private static final Logger logger = LoggerFactory.getLogger(PrivateCredentialsDeserializer.class);

  private final Context                   context;
  private final ObjectMapper              objectMapper;
  private final ChannelSecurityAlgorithms channelSecurityAlgorithms;

  /**
   * Create a new private credentials deserializer.
   *
   * @param solrClient                the solr client.
   * @param objectMapper              the json mapper.
   * @param channelSecurityAlgorithms the channel security algorithms.
   * @param progressTracker           a progress tracking strategy.
   * @param context                   the {@link Context} of the application.
   */
  public PrivateCredentialsDeserializer(SolrClient solrClient,
                                        ObjectMapper objectMapper,
                                        ChannelSecurityAlgorithms channelSecurityAlgorithms,
                                        ProgressTracker progressTracker,
                                        Context context) {
    super(solrClient, UprintSolrCore.PRIVATE_CREDENTIALS, progressTracker);
    this.context = context;
    this.channelSecurityAlgorithms = channelSecurityAlgorithms;
    this.objectMapper = objectMapper;
  }

  @Override
  public ImportReport doImport() {
    Stopwatch stopwatch = Stopwatch.createStarted();
    List<Path> sources = context.getPrinterArchive().getPrivateCredentials();
    AtomicInteger nbrOfProcessedFiles = new AtomicInteger();
    ControlComponentsPublicKeysFile publicKeysFile = context.getControlComponentsPublicKeysFile();
    try {
      splitEpfFilesByControlComponent(sources).forEach((ccIndex, epfFiles) -> {
        List<ControlComponentPublicKey> publicKeys = publicKeysFile.getPublicKeys().get(ccIndex);
        epfFiles.stream()
                .map(path -> decryptAndVerifyEpfFile(path, publicKeys))
                .forEach(credentials -> {
                  deserializeSecretVoterDataList(credentials).forEach(voterData -> {
                    PrivateCredentialsDocument document = createPrivateCredentialsDocument(voterData, ccIndex);
                    next(document, nbrOfProcessedFiles.get(), sources.size());
                  });
                  nbrOfProcessedFiles.incrementAndGet();
                });
      });
    } catch (CredentialsDeserializationException e) {
      abort();
      throw e;
    }
    return new ImportReport(done(), stopwatch.stop().elapsed());
  }

  private Map<Integer, List<Path>> splitEpfFilesByControlComponent(List<Path> epfFiles) {
    return epfFiles.stream()
                   .map(Path::toAbsolutePath)
                   .collect(Collectors.groupingBy(this::extractControlComponentIndex));
  }

  private Integer extractControlComponentIndex(Path epfFile) {
    try {
      return EpfFileName.parse(epfFile).getControlComponentIndex();
    } catch (IllegalArgumentException e) {
      throw new CredentialsDeserializationException("Invalid epf file name: " + epfFile, e);
    }
  }

  private String decryptAndVerifyEpfFile(Path epfFile, List<ControlComponentPublicKey> ccPublicKeys) {
    EncryptedMessageWithSignature message = readEpfFile(epfFile);
    for (ControlComponentPublicKey ccPublicKey : ccPublicKeys) {
      try {
        return channelSecurityAlgorithms.decryptAndVerify(context.getPrinterDecryptionKey().getPrivateKey(),
                                                          ccPublicKey.getPublicKey().getPublicKey(), message);
      } catch (ChannelSecurityException e) {
        logger.debug("Skipping control component public key [{}] ...", ccPublicKey.getId(), e);
      }
    }
    throw new CredentialsDeserializationException("Failed to decrypt/verify the sign-encrypted credentials");
  }

  private EncryptedMessageWithSignature readEpfFile(Path path) {
    try (InputStream in = Files.newInputStream(path)) {
      return objectMapper.readValue(in, EncryptedMessageWithSignature.class);
    } catch (IOException e) {
      throw new CredentialsDeserializationException("Failed to read epf file", e);
    }
  }

  private List<SecretVoterData> deserializeSecretVoterDataList(String serializedCredentials) {
    TypeReference<List<SecretVoterData>> type = new TypeReference<>() {
    };
    try {
      return objectMapper.readValue(serializedCredentials, type);
    } catch (IOException e) {
      throw new CredentialsDeserializationException("Failed to deserialize the secret voter data list", e);
    }
  }

  private PrivateCredentialsDocument createPrivateCredentialsDocument(SecretVoterData voterData, int ccIndex) {
    try {
      return new PrivateCredentialsDocument(voterData.getVoter().getBusinessId(), ccIndex,
                                            objectMapper.writeValueAsString(voterData));
    } catch (IOException e) {
      throw new CredentialsDeserializationException("Failed to serialize credentials", e);
    }
  }
}
