/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.solr.factory;

import ch.ge.ve.uprint.business.solr.api.UprintSolrCore;
import ch.ge.ve.uprint.business.solr.exception.SolrRuntimeException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.solr.client.solrj.embedded.EmbeddedSolrServer;
import org.apache.solr.core.CoreContainer;
import org.springframework.beans.factory.FactoryBean;

/**
 * A utility class to initialize a temporary folder were the solr core will be stored during the execution of the
 * application.
 */
public class SolrServerFactory implements FactoryBean<EmbeddedSolrServer> {

  /**
   * Create a solr server pointing to a core stored in a temporary folder. The solr configuration is copied from the
   * resources of this application.
   *
   * @return the temporary {@link EmbeddedSolrServer}.
   *
   * @throws SolrRuntimeException If there was a problem generating the temporary folder and its files.
   */
  @Override
  public EmbeddedSolrServer getObject() throws Exception {
    Path solrLocation;

    try {
      solrLocation = Files.createTempDirectory("u-print-solr");
    } catch (IOException e) {
      throw new SolrRuntimeException("Failed to create the root temporary directory.", e);
    }

    List<String> resourcesToCopy = new ArrayList<>();
    resourcesToCopy.add("solr.xml");
    Arrays.stream(UprintSolrCore.values()).forEach(core -> {

      try {
        Files.createDirectory(solrLocation.resolve(core.getCoreName()));
        Files.createDirectory(solrLocation.resolve(core.getCoreName() + "/conf"));
      } catch (IOException e) {
        throw new SolrRuntimeException("Cannot create temporary sub-directory", e);
      }

      resourcesToCopy.add(String.format("%s/solrconfig.xml", core.getCoreName()));
      resourcesToCopy.add(String.format("%s/core.properties", core.getCoreName()));
      resourcesToCopy.add(String.format("%s/conf/managed-schema", core.getCoreName()));
    });

    resourcesToCopy.forEach((String resource) -> {
      try (InputStream in = SolrServerFactory.class.getResourceAsStream("/solr/" + resource)) {
        Files.copy(in, solrLocation.resolve(resource));
      } catch (IOException e) {
        throw new SolrRuntimeException(String.format("Cannot copy %s to the temporary directory", resource), e);
      }
    });

    CoreContainer container = new CoreContainer(solrLocation.toAbsolutePath().toString());
    container.load();

    return new EmbeddedSolrServer(container, UprintSolrCore.ELECTORAL_REGISTER.getCoreName());
  }

  @Override
  public Class<?> getObjectType() {
    return EmbeddedSolrServer.class;
  }

  @Override
  public boolean isSingleton() {
    return true;
  }
}
