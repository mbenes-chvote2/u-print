/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.privatecredentials.provider

import ch.ge.ve.javafx.business.progress.ProgressTracker
import ch.ge.ve.protocol.core.algorithm.ChannelSecurityAlgorithms
import ch.ge.ve.uprint.business.context.api.ContextService
import ch.ge.ve.uprint.business.context.model.Context
import ch.ge.ve.uprint.business.solr.api.SolrService
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.solr.client.solrj.SolrClient
import spock.lang.Specification

class PrivateCredentialsDeserializerProviderTest extends Specification {

  SolrService solrService = Mock(SolrService)
  ContextService contextService = Mock(ContextService)
  ObjectMapper objectMapper = Mock(ObjectMapper)
  ChannelSecurityAlgorithms channelSecurityAlgorithms = Mock(ChannelSecurityAlgorithms)
  PrivateCredentialsDeserializerProvider provider

  def setup() {
    provider = new PrivateCredentialsDeserializerProvider(contextService, solrService,
            objectMapper, channelSecurityAlgorithms)
  }

  def "should provide a new instance of PrivateCredentialsDeserializer on every call"() {
    given:
    def tracker = Mock(ProgressTracker)
    def context = Mock(Context)
    contextService.get() >> context
    solrService.getClient() >> Mock(SolrClient)

    when:
    def deserializer = provider.get(tracker)

    then:
    !deserializer.is(provider.get(tracker))
  }
}
