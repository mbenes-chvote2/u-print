/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.gui.controller.format;

import static com.google.common.base.Strings.isNullOrEmpty;

import ch.ge.ve.interfaces.ech.eCH0010.v6.PersonMailAddressInfoType;
import ch.ge.ve.javafx.business.i18n.LanguageUtils;
import ch.ge.ve.javafx.gui.control.StringFormatter;
import com.google.common.base.Strings;
import java.util.MissingResourceException;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.LoggerFactory;

/**
 * Formatter to render a {@link PersonMailAddressInfoType} as a String.
 */
public class PersonInfoFormatter implements StringFormatter<PersonMailAddressInfoType> {

  @Override
  public String asString(PersonMailAddressInfoType person) {
    if (person == null) {
      return "";
    }

    return Stream.of(
        civility(person.getMrMrs()),
        Strings.nullToEmpty(person.getLastName()).toUpperCase(LanguageUtils.getCurrentLocale()),
        person.getFirstName())
                 .filter(personLine -> ! isNullOrEmpty(personLine))
                 .collect(Collectors.joining(" "));
  }

  private String civility(String source) {
    if (isNullOrEmpty(source)) {
      return null;
    }

    final String key = "voting-card.voter.civ." + source;
    try {
      return LanguageUtils.getCurrentResourceBundle().getString(key);
    } catch (MissingResourceException e) {
      LoggerFactory.getLogger(getClass()).warn("Civility unknown for mrMrs=\"{}\"", source, e);
      return "?? mrMrs=" + source + " ??";
    }
  }

}
