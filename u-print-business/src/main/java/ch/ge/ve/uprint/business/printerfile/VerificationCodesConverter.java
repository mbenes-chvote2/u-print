/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.printerfile;

import ch.ge.ve.interfaces.ech.eCH0228.v1.CandidatePositionType;
import ch.ge.ve.interfaces.ech.eCH0228.v1.ElectionType;
import ch.ge.ve.interfaces.ech.eCH0228.v1.VoteType;
import ch.ge.ve.interfaces.ech.eCH0228.v1.VoteType.Ballot;
import ch.ge.ve.interfaces.ech.eCH0228.v1.VoteType.Ballot.StandardBallot;
import ch.ge.ve.interfaces.ech.eCH0228.v1.VoteType.Ballot.VariantBallot;
import ch.ge.ve.interfaces.ech.eCH0228.v1.VotingCardDeliveryType.VotingCard.EVotingIndividualCodes;
import ch.ge.ve.model.convert.model.VoterChoice;
import ch.ge.ve.model.convert.model.VoterElectionChoice;
import ch.ge.ve.model.convert.model.VoterVotationChoice;
import ch.ge.ve.protocol.core.model.VotingCard;
import ch.ge.ve.uprint.business.ech0228.ECH0228CodeDesignation;
import com.google.common.base.Strings;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * A utility class to extract all the verification codes of a {@link VotingCard} as an eCH-0228 {@link
 * EVotingIndividualCodes}.
 */
class VerificationCodesConverter {

  /**
   * Hide utility class constructor
   */
  private VerificationCodesConverter() {
    throw new AssertionError("Not instantiable");
  }

  /**
   * Create a new {@link EVotingIndividualCodes} instance that combines the election metadata of a {@link VoterChoice}
   * and the verification codes in the {@link VotingCard}.
   *
   * @param votingCard   the voting card containing the verification codes.
   * @param voterChoices the election metadata as a list of {@link VoterChoice}.
   *
   * @return an eCH-0228 {@link EVotingIndividualCodes} that contains the combined data.
   */
  static EVotingIndividualCodes toEVotingIndividualCodes(VotingCard votingCard,
                                                         List<VoterChoice> voterChoices) {

    EVotingIndividualCodes verificationCodesType = new EVotingIndividualCodes();

    List<VoteType> voteTypeList = new ArrayList<>();
    List<ElectionType> electionTypeList = new ArrayList<>();

    for (int i = 0; i < voterChoices.size(); i++) {
      VoterChoice choice = voterChoices.get(i);
      String currentVerificationCode = votingCard.getBold_rc().get(i);

      if (!Strings.isNullOrEmpty(currentVerificationCode)) {
        if (choice instanceof VoterVotationChoice) {
          updateVoteTypeList(voteTypeList, (VoterVotationChoice) choice, currentVerificationCode);
        } else if (choice instanceof VoterElectionChoice) {
          updateElectionTypeList(electionTypeList, (VoterElectionChoice) choice, currentVerificationCode);
        }
      }
    }

    verificationCodesType.getVote().addAll(voteTypeList);
    verificationCodesType.getElection().addAll(electionTypeList);

    verificationCodesType.getContestCodes().add(
        ECH0228CodeDesignation.IDENTIFICATION.createNamedCodeType(votingCard.getUpper_x())
    );

    return verificationCodesType;
  }

  private static void updateElectionTypeList(List<ElectionType> electionTypeList,
                                             VoterElectionChoice choice,
                                             String currentVerificationCode) {

    ElectionType electionType = findOrCreateElectionByIdentification(electionTypeList, choice.getVoteId());

    if (choice.isMajorityElection()) {
      if (choice.isEmptyCandidate()) {
        electionType.getEmptyPositionCodes().add(
            createEmptyPositionCodes(electionType, currentVerificationCode)
        );
      } else {
        electionType.getCandidatePosition().add(
            createCandidatePositionType(choice, currentVerificationCode)
        );
      }
    } else if (choice.isProportionalElection()) {
      updateElectoralRoll(electionType, choice, currentVerificationCode);
    }
  }

  private static void updateElectoralRoll(ElectionType electionType,
                                          VoterElectionChoice choice,
                                          String currentVerificationCode) {
    if (choice.isElectoralRoll()) {
      if (choice.isEmptyList()) {
        electionType.getIndividualElectionVerificationCode().add(
            ECH0228CodeDesignation.EMPTY_LIST.createNamedCodeType(currentVerificationCode)
        );
      } else if (choice.isBlankBallot()) {
        electionType.getIndividualElectionVerificationCode().add(
            ECH0228CodeDesignation.BLANK_LIST.createNamedCodeType(currentVerificationCode)
        );
      } else {
        ElectionType.List electoralRoll =
            findOrCreateElectoralRollByIdentifier(electionType.getList(), choice.getCandidateId());
        electoralRoll.getIndividualListVerificationCode().add(
            ECH0228CodeDesignation.VERIFICATION.createNamedCodeType(currentVerificationCode)
        );
      }
    } else {
      if (choice.isEmptyCandidate()) {
        electionType.getEmptyPositionCodes().add(
            createEmptyPositionCodes(electionType, currentVerificationCode)
        );
      } else {
        CandidatePositionType candidatePosition = createCandidatePositionType(choice, currentVerificationCode);

        if (choice.getElectoralRollId() == null) {
          electionType.getCandidatePosition().add(candidatePosition);
        } else {
          ElectionType.List electoralRoll =
              findOrCreateElectoralRollByIdentifier(electionType.getList(), choice.getElectoralRollId());
          electoralRoll.getCandidatePosition().add(candidatePosition);
        }
      }
    }
  }

  private static CandidatePositionType createCandidatePositionType(VoterElectionChoice choice,
                                                                   String currentVerificationCode) {
    CandidatePositionType candidatePositionType = new CandidatePositionType();

    candidatePositionType.setCandidateIdentification(choice.getCandidateId());

    CandidatePositionType.CandidateReference candidateReference = new CandidatePositionType.CandidateReference();
    candidateReference.setOccurence(BigInteger.valueOf(1));
    candidateReference.setCandidateVerificationCode(currentVerificationCode);

    candidatePositionType.getCandidateReference().add(candidateReference);

    return candidatePositionType;
  }

  private static ElectionType.EmptyPositionCodes createEmptyPositionCodes(ElectionType electionType,
                                                                          String currentVerificationCode) {

    ElectionType.EmptyPositionCodes emptyPositionCodes = new ElectionType.EmptyPositionCodes();

    emptyPositionCodes.setPosition(BigInteger.valueOf(electionType.getEmptyPositionCodes().size() + 1L));
    emptyPositionCodes.setEmptyPositionVerificationCode(currentVerificationCode);

    return emptyPositionCodes;
  }

  private static ElectionType.List findOrCreateElectoralRollByIdentifier(List<ElectionType.List> electoralRolls,
                                                                         String electoralRollId) {

    return electoralRolls.stream()
                         .filter(list -> electoralRollId.equals(list.getListIdentification()))
                         .findFirst()
                         .orElseGet(() -> {
                           ElectionType.List electoralRoll = new ElectionType.List();
                           electoralRoll.setListIdentification(electoralRollId);
                           electoralRolls.add(electoralRoll);
                           return electoralRoll;
                         });
  }


  private static ElectionType findOrCreateElectionByIdentification(List<ElectionType> elections,
                                                                   String electionIdentification) {
    return elections.stream()
                    .filter(election -> electionIdentification.equals(election.getElectionIdentification()))
                    .findFirst()
                    .orElseGet(() -> {
                      ElectionType electionType = new ElectionType();
                      electionType.setElectionIdentification(electionIdentification);
                      elections.add(electionType);

                      return electionType;
                    });
  }

  private static void updateVoteTypeList(List<VoteType> voteTypeList,
                                         VoterVotationChoice choice,
                                         String currentVerificationCode) {
    VoteType voteType = findOrCreateVoteByIdentification(voteTypeList, choice.getVoteId());
    Ballot ballot = findOrCreateBallotByIdentifier(voteType, choice.getBallotId());
    updateBallot(ballot, choice, currentVerificationCode);
  }

  private static VoteType findOrCreateVoteByIdentification(List<VoteType> votes, String voteIdentification) {
    return votes.stream()
                .filter(vote -> voteIdentification.equals(vote.getVoteIdentification()))
                .findFirst()
                .orElseGet(() -> {
                  VoteType voteType = new VoteType();
                  voteType.setVoteIdentification(voteIdentification);
                  votes.add(voteType);

                  return voteType;
                });
  }

  private static Ballot findOrCreateBallotByIdentifier(VoteType voteType, String ballotIdentifier) {
    return voteType.getBallot().stream()
                   .filter(ballot -> ballotIdentifier.equals(ballot.getBallotIdentification()))
                   .findFirst()
                   .orElseGet(() -> {
                     Ballot ballot = new Ballot();
                     ballot.setBallotIdentification(ballotIdentifier);
                     voteType.getBallot().add(ballot);
                     return ballot;
                   });
  }

  private static VariantBallot.Question findOrCreateQuestionByIdentifier(VariantBallot variantBallot,
                                                                         String questionIdentifier) {
    return variantBallot.getQuestion().stream()
                        .filter(question -> questionIdentifier.equals(question.getQuestionIdentification()))
                        .findFirst()
                        .orElseGet(() -> {
                          VariantBallot.Question question = new VariantBallot.Question();
                          question.setQuestionIdentification(questionIdentifier);
                          variantBallot.getQuestion().add(question);
                          return question;
                        });
  }

  private static StandardBallot.Question createStandardBallotQuestion(VoterChoice choice) {
    StandardBallot.Question question = new StandardBallot.Question();
    question.setQuestionIdentification(choice.getElection().getIdentifier());
    return question;
  }

  private static void updateBallot(Ballot ballot, VoterVotationChoice choice, String currentVerificationCode) {
    if (choice.isStandardBallot()) {

      StandardBallot standardBallot = ballot.getStandardBallot();

      if (standardBallot == null) {
        standardBallot = new StandardBallot();
        standardBallot.setQuestion(createStandardBallotQuestion(choice));
        ballot.setStandardBallot(standardBallot);
      }

      standardBallot.getQuestion().getAnswerOption().add(
          createStandardAnswerOption(choice, currentVerificationCode)
      );

    } else if (choice.isVariantBallot()) {

      VariantBallot variantBallot = ballot.getVariantBallot();

      if (variantBallot == null) {
        variantBallot = new VariantBallot();
        ballot.setVariantBallot(variantBallot);
      }

      VariantBallot.Question question =
          findOrCreateQuestionByIdentifier(variantBallot, choice.getElection().getIdentifier());
      question.getAnswerOption().add(
          createVariantAnswerOption(choice, currentVerificationCode)
      );

    } else {
      throw new IllegalStateException("Unsupported ballot type");
    }

  }

  private static StandardBallot.Question.AnswerOption createStandardAnswerOption(
      VoterVotationChoice choice, String currentVerificationCode) {

    StandardBallot.Question.AnswerOption option = new StandardBallot.Question.AnswerOption();
    option.setAnswerIdentification(String.valueOf(choice.getAnswerRank()));
    option.setAnswerSequenceNumber(BigInteger.valueOf(choice.getAnswerRank()));
    option.setIndividualAnswerVerificationCode(currentVerificationCode);

    return option;
  }

  private static VariantBallot.Question.AnswerOption createVariantAnswerOption(
      VoterVotationChoice choice, String currentVerificationCode) {

    VariantBallot.Question.AnswerOption option = new VariantBallot.Question.AnswerOption();
    option.setAnswerIdentification(String.valueOf(choice.getAnswerRank()));
    option.setAnswerSequenceNumber(BigInteger.valueOf(choice.getAnswerRank()));
    option.setIndividualAnswerVerificationCode(currentVerificationCode);

    return option;
  }
}
