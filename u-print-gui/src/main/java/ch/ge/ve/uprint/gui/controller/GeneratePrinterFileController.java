/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.gui.controller;

import ch.ge.ve.filenamer.PrinterArchiveFileName;
import ch.ge.ve.javafx.business.progress.ProgressTracker;
import ch.ge.ve.javafx.gui.control.MessageContainer;
import ch.ge.ve.javafx.gui.progress.ProgressBarTracker;
import ch.ge.ve.javafx.gui.utils.FileBrowserService;
import ch.ge.ve.javafx.gui.utils.TaskUtils;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.uprint.business.context.api.ContextService;
import ch.ge.ve.uprint.business.context.factory.PrivateKeyFactory;
import ch.ge.ve.uprint.business.ech0045.provider.ECH0045ParserProvider;
import ch.ge.ve.uprint.business.printerfile.PrinterFileGenerator;
import ch.ge.ve.uprint.business.privatecredentials.provider.PrivateCredentialsDeserializerProvider;
import ch.ge.ve.uprint.business.solr.api.SolrService;
import ch.ge.ve.uprint.business.solr.api.UprintSolrCore;
import ch.ge.ve.uprint.business.solr.model.ImportReport;
import ch.ge.ve.uprint.gui.controller.exception.TaskExecutionException;
import com.google.common.base.Stopwatch;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXProgressBar;
import com.jfoenix.controls.JFXTextField;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

/**
 * JavaFX Controller for the printer file generation.
 */
@Component
public class GeneratePrinterFileController {
  private static final String ERROR_TRANSLATION_KEY = "generate-printer-file.exception";

  private static final Logger logger = LoggerFactory.getLogger(GeneratePrinterFileController.class);

  private final FileBrowserService                     fileBrowserService;
  private final PrivateKeyFactory                      privateKeyFactory;
  private final ContextService                         contextService;
  private final ECH0045ParserProvider                  ech0045ParserProvider;
  private final PrivateCredentialsDeserializerProvider privateCredentialsDeserializerProvider;
  private final PrinterFileGenerator                   printerFileGenerator;
  private final SolrService                            solrService;
  private final TaskExecutor                           taskExecutor;
  private final ApplicationState                       applicationState;

  private CountDownLatch isContextReadySignal;
  private CountDownLatch isImportReadySignal;

  @FXML
  private StackPane root;

  @FXML
  private MessageContainer errorMessageContainer;

  @FXML
  private VBox importBox;

  @FXML
  private HBox actionBox;

  @FXML
  private VBox progressBox;

  @FXML
  private JFXDialog dialog;

  @FXML
  private JFXTextField printerArchivePath;

  @FXML
  private JFXTextField privateKeyPath;

  @FXML
  private JFXTextField destinationPath;

  @FXML
  private JFXPasswordField privateKeyPassphrase;

  @FXML
  private HBox extractPrinterArchiveProgressRow;

  @FXML
  private JFXProgressBar extractPrinterArchiveProgress;

  @FXML
  private HBox votersRegisterProgressRow;

  @FXML
  private HBox votersRegisterResultsRow;

  @FXML
  private JFXProgressBar votersRegisterProgress;

  @FXML
  private HBox privateCredentialsProgressRow;

  @FXML
  private JFXProgressBar privateCredentialsProgress;

  @FXML
  private HBox printerFileGenerationProgressRow;

  @FXML
  private JFXProgressBar printerFileGenerationProgress;

  @Autowired
  public GeneratePrinterFileController(FileBrowserService fileBrowserService,
                                       ContextService contextService,
                                       PrivateKeyFactory privateKeyFactory,
                                       ECH0045ParserProvider ech0045ParserProvider,
                                       PrivateCredentialsDeserializerProvider privateCredentialsDeserializerProvider,
                                       PrinterFileGenerator printerFileGenerator,
                                       SolrService solrService,
                                       TaskExecutor taskExecutor,
                                       ApplicationState applicationState) {
    this.fileBrowserService = fileBrowserService;
    this.contextService = contextService;
    this.privateKeyFactory = privateKeyFactory;
    this.ech0045ParserProvider = ech0045ParserProvider;
    this.privateCredentialsDeserializerProvider = privateCredentialsDeserializerProvider;
    this.printerFileGenerator = printerFileGenerator;
    this.solrService = solrService;
    this.taskExecutor = taskExecutor;
    this.applicationState = applicationState;
  }

  /**
   * The {@link JFXDialog} implementation adds and removes the dialog from its container whenever the {@link
   * JFXDialog#show(StackPane)} and {@link JFXDialog#close()} are called. In order to create the dialog in the FXML
   * directly we need to manually remove it from its container on the initialization stage so that it works as
   * expected.
   *
   * @see JFXDialog#show(StackPane)
   * @see JFXDialog#close()
   */
  @FXML
  public void initialize() {
    root.getChildren().remove(dialog);

    // Give focus to the password field when the dialog opens.
    dialog.setOnDialogOpened(event -> privateKeyPassphrase.requestFocus());

    // ENTER submits the private key passphrase.
    privateKeyPassphrase.setOnKeyPressed(event -> {
      if (event.getCode() == KeyCode.ENTER) {
        doGeneration();
      }
    });
  }


  /**
   * Open the file chooser dialog to select a printer archive.
   */
  @FXML
  public void selectPrinterArchiveFile() {
    fileBrowserService.selectFile(printerArchivePath, PrinterArchiveFileName.EXTENSION);
  }

  /**
   * Open the file chooser dialog to select the printer's private key.
   */
  @FXML
  public void selectPrivateKeyFile() {
    fileBrowserService.selectFile(privateKeyPath, "pfx");
  }

  /**
   * Open the file chooser dialog to select a destination folder for the generated printer file,
   */
  @FXML
  public void selectDestinationFolder() {
    fileBrowserService.selectDirectory(destinationPath);
  }

  /**
   * Show the passphrase dialog to confirm or to cancel the generation of the printer file.
   */
  @FXML
  public void showPassphraseDialog() {
    if (printerArchivePath.validate() &&
        privateKeyPath.validate() &&
        destinationPath.validate()) {
      dialog.show(root);
    }
  }

  /**
   * Close the passphrase dialog and cancels the generation.
   */
  @FXML
  public void cancelGeneration() {
    dialog.close();
    privateKeyPassphrase.clear();
    privateKeyPassphrase.resetValidation();
  }

  /**
   * Close the passphrase dialog and performs the generation.
   */
  @FXML
  public void doGeneration() {
    if (privateKeyPassphrase.validate()) {

      dialog.close();

      TaskUtils.clearAllIcons(extractPrinterArchiveProgressRow,
                              privateCredentialsProgressRow,
                              votersRegisterProgressRow,
                              printerFileGenerationProgressRow);

      applicationState.setPrinterFileGenerationInProgress(true);

      importBox.setVisible(false);
      importBox.setManaged(false);
      actionBox.setVisible(false);
      actionBox.setManaged(false);
      progressBox.setVisible(true);
      progressBox.setManaged(true);

      isContextReadySignal = new CountDownLatch(1);
      isImportReadySignal = new CountDownLatch(2);

      prepareContext();
      doImportPrivateCredentials();
      doImportPrinterArchive();
      generatePrinterFile();
    }
  }

  private <T> void onTaskFailed(Task<T> task, Pane progressBarContainer, JFXProgressBar progressBar) {
    TaskUtils.onFailed(task, progressBarContainer);
    errorMessageContainer.setMessage(TaskUtils.formatErrorMessage(ERROR_TRANSLATION_KEY, task));
    Platform.runLater(() -> progressBar.setProgress(1));
  }

  private void prepareContext() {
    Task<Duration> task = new Task<>() {
      @Override
      protected Duration call() {
        Stopwatch stopwatch = Stopwatch.createStarted();

        contextService.clearPrinterArchiveContext();
        solrService.delete(UprintSolrCore.ELECTORAL_REGISTER, UprintSolrCore.PRIVATE_CREDENTIALS);

        IdentificationPrivateKey printerPrivateKey = privateKeyFactory.readPrivateKey(
            Paths.get(privateKeyPath.getText()),
            privateKeyPassphrase.getText().toCharArray()
        );

        contextService.setupPrinterArchiveContext(
            printerPrivateKey,
            Paths.get(printerArchivePath.getText()),
            Paths.get(destinationPath.getText()),
            new ProgressBarTracker(extractPrinterArchiveProgress)
        );

        return stopwatch.stop().elapsed();
      }
    };

    task.setOnFailed(event -> {
      logger.error("Failed to prepare the context", event.getSource().getException());
      onPrepareContextFailed(task);
    });

    task.setOnSucceeded(event -> {
      try {
        TaskUtils.onSucceeded(extractPrinterArchiveProgressRow);
        logger.info("Printer archive preparation took [{}] seconds", task.get().getSeconds());
      } catch (ExecutionException e) {
        logger.error("Cannot retrieve the result of the prepare context task", e);
        onPrepareContextFailed(task);
        throw new TaskExecutionException("Cannot retrieve the result of the prepare context task", e);
      } catch (InterruptedException e) {
        logger.error("The prepare context task was interrupted", e);
        onPrepareContextFailed(task);
        Thread.currentThread().interrupt();
      } finally {
        isContextReadySignal.countDown();
      }
    });

    taskExecutor.execute(task);
  }

  private <T> void onPrepareContextFailed(Task<T> task) {
    onTaskFailed(task, extractPrinterArchiveProgressRow, extractPrinterArchiveProgress);
    contextService.get().setPrinterFile(null);
    isContextReadySignal.countDown();
  }

  private void doImportPrivateCredentials() {
    Task<ImportReport> task = new Task<>() {
      @Override
      protected ImportReport call() throws Exception {
        isContextReadySignal.await();
        ProgressTracker progressTracker = new ProgressBarTracker(privateCredentialsProgress);
        return privateCredentialsDeserializerProvider.get(progressTracker).doImport();
      }
    };

    task.setOnFailed(event -> {
      logger.error("Failed to import private credentials", event.getSource().getException());
      onImportPrivateCredentialsFailed(task);
    });

    task.setOnSucceeded(event -> {
      try {
        TaskUtils.onSucceeded(privateCredentialsProgressRow);
        logger.info("Private credentials import took [{}] seconds", task.get().getElapsedTime().getSeconds());
      } catch (ExecutionException e) {
        logger.error("Cannot retrieve the result of the private credentials import task", e);
        onImportPrivateCredentialsFailed(task);
        throw new TaskExecutionException("Cannot retrieve the result of the private credentials import task", e);
      } catch (InterruptedException e) {
        logger.error("The private credentials import task was interrupted", e);
        onImportPrivateCredentialsFailed(task);
        Thread.currentThread().interrupt();
      } finally {
        isImportReadySignal.countDown();
      }
    });

    taskExecutor.execute(task);
  }

  private <T> void onImportPrivateCredentialsFailed(Task<T> task) {
    onTaskFailed(task, privateCredentialsProgressRow, privateCredentialsProgress);
    isImportReadySignal.countDown();
  }

  private void doImportPrinterArchive() {
    Task<ImportReport> task = new Task<>() {
      @Override
      protected ImportReport call() throws Exception {
        isContextReadySignal.await();
        ProgressTracker progressTracker = new ProgressBarTracker(votersRegisterProgress);
        return ech0045ParserProvider.get(progressTracker).doImport();
      }
    };

    task.setOnFailed(event -> {
      logger.error("Failed to import printer archive", event.getSource().getException());
      onImportPrinterArchiveFailed(task);
    });

    task.setOnSucceeded(event -> {
      try {
        ImportReport report = task.get();
        TaskUtils.onSucceeded(votersRegisterProgressRow);

        displayVoterRegisterResultRow(report.getNbrOfDocuments());

        logger.info("Election register import took [{}] seconds", report.getElapsedTime().getSeconds());
      } catch (ExecutionException e) {
        logger.error("Cannot retrieve the result of the printer archive import task", e);
        onImportPrinterArchiveFailed(task);
        throw new TaskExecutionException("Cannot retrieve the result of the printer archive import task", e);
      } catch (InterruptedException e) {
        logger.error("The printer archive import task was interrupted", e);
        onImportPrinterArchiveFailed(task);
        Thread.currentThread().interrupt();
      } finally {
        isImportReadySignal.countDown();
      }
    });

    taskExecutor.execute(task);
  }

  private <T> void onImportPrinterArchiveFailed(Task<T> task) {
    onTaskFailed(task, votersRegisterProgressRow, votersRegisterProgress);
    isImportReadySignal.countDown();
  }

  private void displayVoterRegisterResultRow(Long nbrOfVoters) {
    votersRegisterResultsRow.getChildren().add(new Label(nbrOfVoters.toString()));
    votersRegisterResultsRow.setVisible(true);
    votersRegisterResultsRow.setManaged(true);
  }

  private void generatePrinterFile() {
    Task<Duration> task = new Task<>() {
      @Override
      protected Duration call() throws Exception {
        isImportReadySignal.await();
        Stopwatch stopwatch = Stopwatch.createStarted();
        ProgressTracker progressTracker = new ProgressBarTracker(printerFileGenerationProgress);

        printerFileGenerator.generatePrinterFile(
            contextService.get().getPrinterArchive(),
            contextService.get().getPrinterFileDestination(),
            progressTracker
        );

        return stopwatch.stop().elapsed();
      }
    };

    task.setOnFailed(event -> {
      logger.error("Failed to generate printer file", event.getSource().getException());
      onGeneratePrinterFile(task);
    });

    task.setOnSucceeded(event -> {
      try {
        TaskUtils.onSucceeded(printerFileGenerationProgressRow);
        logger.info("Private file generation took [{}] seconds", task.get().getSeconds());
      } catch (ExecutionException e) {
        logger.error("Cannot retrieve the result of the printer file generation task", e);
        onGeneratePrinterFile(task);
        throw new TaskExecutionException("Cannot retrieve the result of the printer file generation task", e);
      } catch (InterruptedException e) {
        logger.error("The printer file generation task was interrupted", e);
        onGeneratePrinterFile(task);
        Thread.currentThread().interrupt();
      } finally {
        applicationState.setPrinterFileGenerationInProgress(false);
      }
    });

    taskExecutor.execute(task);
  }

  private <T> void onGeneratePrinterFile(Task<T> task) {
    onTaskFailed(task, printerFileGenerationProgressRow, printerFileGenerationProgress);
    applicationState.setPrinterFileGenerationInProgress(false);
  }
}
