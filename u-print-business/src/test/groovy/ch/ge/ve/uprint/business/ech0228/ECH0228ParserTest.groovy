/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.ech0228

import ch.ge.ve.javafx.business.progress.ProgressTracker
import ch.ge.ve.uprint.business.TestResources
import ch.ge.ve.uprint.business.solr.EmbeddedSolrService
import ch.ge.ve.uprint.business.solr.api.SolrService
import ch.ge.ve.uprint.business.solr.api.UprintSolrCore
import ch.ge.ve.uprint.business.solr.factory.SolrServerFactory
import ch.ge.ve.uprint.business.solr.model.VotingCardDocument
import org.apache.solr.client.solrj.SolrQuery
import spock.lang.Specification

class ECH0228ParserTest extends Specification {
  SolrService solrService

  def setup() {
    solrService = new EmbeddedSolrService(new SolrServerFactory().getObject())
  }

  def cleanup() {
    solrService.deleteAll()
    solrService.close()
  }

  def "should import 11 voters"() {
    given:
    def solrServer = solrService.getClient()
    def progressTracker = Mock(ProgressTracker)
    def printerFile = TestResources.ECH0228
    def parser = new ECH0228Parser(solrServer, progressTracker, printerFile)

    when:
    def importReport = parser.doImport()
    def response = solrServer.query(UprintSolrCore.VOTING_CARDS.getCoreName(), new SolrQuery("*:*"))
    def result = response.getBeans(VotingCardDocument.class)

    then:
    response.results.getNumFound() == 11
    importReport.getNbrOfDocuments() == 11
    result.get(0).getVoterId() == "0ohQ5VPXbtiFbq2XDSOGJ4uJ_MT0cr-yaPPC"
  }
}
