/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.printerfile

import ch.ge.ve.javafx.business.json.JsonPathMapper
import ch.ge.ve.javafx.business.progress.ProgressTracker
import ch.ge.ve.javafx.business.xml.HeaderTypeFactory
import ch.ge.ve.javafx.business.xml.VoterChoicesFactory
import ch.ge.ve.model.convert.impl.DefaultVoterChoiceConverter
import ch.ge.ve.protocol.core.algorithm.ChannelSecurityAlgorithms
import ch.ge.ve.protocol.model.PublicParameters
import ch.ge.ve.uprint.business.ArchiveHelper
import ch.ge.ve.uprint.business.ContextHelper
import ch.ge.ve.uprint.business.CryptoTestConfig
import ch.ge.ve.uprint.business.TestResources
import ch.ge.ve.uprint.business.context.factory.ControlComponentsPublicKeysFileFactory
import ch.ge.ve.uprint.business.context.factory.PrinterArchiveFactory
import ch.ge.ve.uprint.business.context.factory.PrivateKeyFactory
import ch.ge.ve.uprint.business.context.model.Context
import ch.ge.ve.uprint.business.ech0045.ECH0045Parser
import ch.ge.ve.uprint.business.privatecredentials.PrivateCredentialsDeserializer
import ch.ge.ve.uprint.business.solr.EmbeddedSolrService
import ch.ge.ve.uprint.business.solr.api.SolrService
import ch.ge.ve.uprint.business.solr.factory.SolrServerFactory
import com.fasterxml.jackson.databind.ObjectMapper
import java.nio.file.Files
import java.nio.file.Path
import java.util.zip.ZipInputStream
import org.apache.solr.client.solrj.SolrClient
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

@ContextConfiguration(classes = [CryptoTestConfig])
class PrinterFileGeneratorTest extends Specification {

  @Rule
  TemporaryFolder temporaryFolder = new TemporaryFolder()

  @Autowired
  PublicParameters publicParameters

  @Autowired
  ObjectMapper objectMapper

  @Autowired
  JsonPathMapper jsonPathMapper

  @Autowired
  ChannelSecurityAlgorithms channelSecurityAlgorithms

  SolrService solrService
  VoterChoicesFactory voterChoicesFactory
  HeaderTypeFactory headerTypeFactory
  ContextHelper contextHelper
  Path outputDir

  def setup() {
    solrService = new EmbeddedSolrService(new SolrServerFactory().getObject())
    voterChoicesFactory = new VoterChoicesFactory(new DefaultVoterChoiceConverter())
    headerTypeFactory = new HeaderTypeFactory("DGSI - SID", "u-print", "0.0.2", "voting cards")
    contextHelper = new ContextHelper(
            new ControlComponentsPublicKeysFileFactory(publicParameters, jsonPathMapper),
            new PrinterArchiveFactory(),
            new PrivateKeyFactory(publicParameters),
            new ArchiveHelper(temporaryFolder)
    )
    outputDir = temporaryFolder.newFolder("printer-file").toPath()
  }

  def cleanup() {
    solrService.deleteAll()
    solrService.close()
  }


  def "should generate printer file with 11 voting cards for a standard ballot"() {
    given:
    def context = contextHelper.createContext(
            TestResources.PRINTER_ARCHIVE_VALID_STANDARD_BALLOT,
            outputDir,
            Mock(ProgressTracker)
    )
    prepareSolrIndex(solrService.getClient(), context)
    def printerFileGenerator = createPrinterFileGenerator(0, false)


    when:
    printerFileGenerator.generatePrinterFile(context.getPrinterArchive(), outputDir, Mock(ProgressTracker))

    then: 'the generated archive should contain the expected files'
    def zipOutput = extractGeneratedFile()
    def content = zipOutput.toFile().list().sort()
    content.length == 2
    content[0] == "eCH-0159-standard-ballot.xml"
    content[1] =~ "^eCH-0228_([\\w-]+)_([\\w-]+)_([\\w-]+)\\.xml\$"

    and: 'the eCH-0228 should contain the correct operation data'
    def delivery = new XmlSlurper().parse(zipOutput.resolve(content[1]).toFile())
    delivery.votingCardDelivery.contestData.contestIdentification.text() == "201901VP"
    delivery.votingCardDelivery.contestData.eVotingContestCodes.find {
      it.codeDesignation == "VOTING_CARD_TITLE"
    }.codeValue == "Test cards"
    delivery.votingCardDelivery.votingCard.size() == 11
    delivery.votingCardDelivery.votingCard.first().votingCardsequenceNumber == "0"
    delivery.votingCardDelivery.votingCard.last().votingCardsequenceNumber == "10"
  }

  def "should generate printer file with 451 voting cards for a variant ballot"() {
    given:
    def context = contextHelper.createContext(
            TestResources.PRINTER_ARCHIVE_VALID_VARIANT_BALLOT,
            outputDir,
            Mock(ProgressTracker)
    )
    prepareSolrIndex(solrService.getClient(), context)
    def printerFileGenerator = createPrinterFileGenerator(1000000, false)

    when:
    printerFileGenerator.generatePrinterFile(context.getPrinterArchive(), outputDir, Mock(ProgressTracker))

    then:
    def zipOutput = extractGeneratedFile()
    def printerFile = zipOutput.toFile().listFiles().find { it.name.startsWith("eCH-0228") }

    def delivery = new XmlSlurper().parse(printerFile)
    delivery.votingCardDelivery.contestData.contestIdentification.text() == "201901VP"
    delivery.votingCardDelivery.contestData.eVotingContestCodes.find {
      it.codeDesignation == "VOTING_CARD_TITLE"
    }.codeValue == "Test cards"
    delivery.votingCardDelivery.votingCard.size() == 451
    delivery.votingCardDelivery.votingCard.first().votingCardsequenceNumber == "1000000"
    delivery.votingCardDelivery.votingCard.last().votingCardsequenceNumber == "1000450"
  }

  def "should generate printer file with 16 voting cards with multiple operation reference files"() {
    given:
    def context = contextHelper.createContext(
            TestResources.PRINTER_ARCHIVE_VALID_MULTIPLE_OP_REF,
            outputDir,
            Mock(ProgressTracker)
    )
    prepareSolrIndex(solrService.getClient(), context)
    def printerFileGenerator = createPrinterFileGenerator(0, false)

    when:
    printerFileGenerator.generatePrinterFile(context.getPrinterArchive(), outputDir, Mock(ProgressTracker))

    then:
    def zipOutput = extractGeneratedFile()
    def printerFile = zipOutput.toFile().listFiles().find { it.name.startsWith("eCH-0228") }

    def delivery = new XmlSlurper().parse(printerFile)
    delivery.votingCardDelivery.contestData.contestIdentification.text() == "202002VP"
    delivery.votingCardDelivery.contestData.eVotingContestCodes.find {
      it.codeDesignation == "VOTING_CARD_TITLE"
    }.codeValue == "Test cards"
    delivery.votingCardDelivery.votingCard.size() == 16
    delivery.votingCardDelivery.votingCard.first().votingCardsequenceNumber == "0"
    delivery.votingCardDelivery.votingCard.last().votingCardsequenceNumber == "15"
  }

  def "should generate printer file with 19 voting cards for a majority election"() {
    given:
    def context = contextHelper.createContext(
            TestResources.PRINTER_ARCHIVE_VALID_MAJORITY_ELECTION,
            outputDir,
            Mock(ProgressTracker)
    )
    prepareSolrIndex(solrService.getClient(), context)
    def printerFileGenerator = createPrinterFileGenerator(0, false)

    when:
    printerFileGenerator.generatePrinterFile(context.getPrinterArchive(), outputDir, Mock(ProgressTracker))

    then:
    def zipOutput = extractGeneratedFile()
    def printerFile = zipOutput.toFile().listFiles().find { it.name.startsWith("eCH-0228") }

    def delivery = new XmlSlurper().parse(printerFile)
    delivery.votingCardDelivery.contestData.contestIdentification.text() == "202005CE"
    delivery.votingCardDelivery.contestData.eVotingContestCodes.find {
      it.codeDesignation == "VOTING_CARD_TITLE"
    }.codeValue == "Test cards"
    delivery.votingCardDelivery.votingCard.size() == 10
    delivery.votingCardDelivery.votingCard.first().votingCardsequenceNumber == "0"
    delivery.votingCardDelivery.votingCard.last().votingCardsequenceNumber == "9"
  }

  def "should generate printer file with 10 voting cards for a proportional election"() {
    given:
    def context = contextHelper.createContext(
            TestResources.PRINTER_ARCHIVE_VALID_PROPORTIONAL_ELECTION,
            outputDir,
            Mock(ProgressTracker)
    )
    prepareSolrIndex(solrService.getClient(), context)
    def printerFileGenerator = createPrinterFileGenerator(0, false)


    when:
    printerFileGenerator.generatePrinterFile(context.getPrinterArchive(), outputDir, Mock(ProgressTracker))

    then:
    def zipOutput = extractGeneratedFile()
    def printerFile = zipOutput.toFile().listFiles().find { it.name.startsWith("eCH-0228") }

    def delivery = new XmlSlurper().parse(printerFile)
    delivery.votingCardDelivery.contestData.contestIdentification.text() == "201804EC"
    delivery.votingCardDelivery.contestData.eVotingContestCodes.find {
      it.codeDesignation == "VOTING_CARD_TITLE"
    }.codeValue == "Test cards"
    delivery.votingCardDelivery.votingCard.size() == 10
    delivery.votingCardDelivery.votingCard.first().votingCardsequenceNumber == "0"
    delivery.votingCardDelivery.votingCard.last().votingCardsequenceNumber == "9"
  }

  def extractGeneratedFile() {
    def generated = Files.list(outputDir)
            .filter({ Path path -> path.getFileName().toString().endsWith(".zip") })
            .findAny().orElseThrow({ new AssertionError("No generated file found") })

    def output = Files.createTempDirectory("u-print-test-printer-file-extracted")
    def zin = new ZipInputStream(Files.newInputStream(generated))
    def entry = zin.getNextEntry()
    while (entry != null) {
      Files.copy(zin, output.resolve(entry.name))
      entry = zin.getNextEntry()
    }
    return output
  }

  def prepareSolrIndex(SolrClient solrServer, Context context) {
    def register = context.getPrinterArchive().getEch0045()
    def parser = new ECH0045Parser(solrServer, Mock(ProgressTracker), register)
    parser.doImport()
    def deserializer = new PrivateCredentialsDeserializer(
            solrServer, objectMapper, channelSecurityAlgorithms, Mock(ProgressTracker), context
    )
    deserializer.doImport()
  }

  def createPrinterFileGenerator(int initialSequenceNbr, boolean useVoterIdAsSequenceNumber) {
    return new PrinterFileGenerator(
            solrService,
            objectMapper,
            jsonPathMapper,
            headerTypeFactory,
            voterChoicesFactory,
            initialSequenceNbr,
            useVoterIdAsSequenceNumber)
  }
}
