/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.solr.api;

import java.io.Closeable;
import org.apache.solr.client.solrj.SolrClient;

/**
 * A service to interact with a solr server.
 */
public interface SolrService extends Closeable {
  /**
   * Get the solr client of the underlying server.
   *
   * @return the {@link SolrClient}.
   */
  SolrClient getClient();

  /**
   * Wipe all the data for all known solr cores.
   *
   * @see UprintSolrCore
   */
  void deleteAll();

  /**
   * Wipe all the data for all the given solr cores.
   *
   * @param cores the cores to delete.
   *
   * @see UprintSolrCore
   */
  void delete(UprintSolrCore... cores);
}
