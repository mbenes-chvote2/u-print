/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.ech0045;

import ch.ge.ve.interfaces.ech.eCH0045.v4.PersonType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.VotingPersonType;

/**
 * A utility class that contains common functionality to retrieve information from eCH-0045 entities.
 */
class ECH0045Utils {

  /**
   * Retrieve the person id of an eCH-0045 {@link VotingPersonType} entity.
   *
   * @param votingPerson the eCH-0045 {@link VotingPersonType} entity.
   *
   * @return the person id.
   */
  static String retrievePersonId(VotingPersonType votingPerson) {
    VotingPersonType.Person person = votingPerson.getPerson();

    PersonType personType;
    if (person.getForeigner() != null) {
      personType = person.getForeigner().getForeignerPerson();
    } else if (person.getSwissAbroad() != null) {
      personType = person.getSwissAbroad().getSwissAbroadPerson();
    } else if (person.getSwiss() != null) {
      personType = person.getSwiss().getSwissDomesticPerson();
    } else {
      throw new IllegalArgumentException("The person type must contain an identification");
    }

    return personType.getPersonIdentification().getLocalPersonId().getPersonId();
  }

  /**
   * Hide utility class constructor.
   */
  private ECH0045Utils() {
    throw new AssertionError("Not instantiable");
  }

}
