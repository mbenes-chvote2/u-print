/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.solr.api;

/**
 * An enum containing all the solr cores of the U-Print application.
 */
public enum UprintSolrCore {
  /**
   * The electoral ech0045 core.
   */
  ELECTORAL_REGISTER("electoralRegister"),
  /**
   * The private credentials core.
   */
  PRIVATE_CREDENTIALS("privateCredentials"),
  /**
   * The voting cards core.
   */
  VOTING_CARDS("votingCards");

  private final String coreName;

  UprintSolrCore(String coreName) {
    this.coreName = coreName;
  }

  /**
   * Return the core name of this solr core. Use the value returned by this function as a reference on all the calls to
   * solr.
   *
   * @return the core name of this solr core.
   */
  public String getCoreName() {
    return coreName;
  }
}