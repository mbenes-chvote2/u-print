/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.context.factory;

import ch.ge.ve.crypto.PrivateKeyStore;
import ch.ge.ve.crypto.exception.CryptoException;
import ch.ge.ve.crypto.impl.PrivateKeyStorePkcs12Impl;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.protocol.model.IdentificationGroup;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.uprint.business.context.exception.InvalidPrivateKeyException;
import ch.ge.ve.uprint.business.context.exception.InvalidPrivateKeyException.PrivateKeyErrorCode;
import java.math.BigInteger;
import java.nio.file.Path;
import java.security.PrivateKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * A {@link PrivateKey} provider.
 */
@Service
public class PrivateKeyFactory {

  private static final Logger logger = LoggerFactory.getLogger(PrivateKeyFactory.class);

  private final IdentificationGroup identificationGroup;

  /**
   * Create a new factory instance with the given parameters.
   *
   * @param publicParameters the voting protocol public parameters.
   */
  @Autowired
  public PrivateKeyFactory(PublicParameters publicParameters) {
    this.identificationGroup = publicParameters.getIdentificationGroup();
  }

  /**
   * Read an {@link IdentificationPrivateKey} from the specified key store.
   *
   * @param source   the location of the keystore.
   * @param password the password to use to unlock the keystore.
   *
   * @return the read {@link IdentificationPrivateKey} instance.
   *
   * @throws InvalidPrivateKeyException if there was a problem extracting the private key.
   */
  public IdentificationPrivateKey readPrivateKey(Path source, char[] password) {
    PrivateKeyStore keyStore = new PrivateKeyStorePkcs12Impl(source);
    try {
      BigInteger privateKey = new BigInteger(keyStore.loadKey(password));
      return new IdentificationPrivateKey(privateKey, identificationGroup);
    } catch (CryptoException e) {
      logger.error("File [{}] is not a valid key store or the given password does not match", source);
      throw new InvalidPrivateKeyException(PrivateKeyErrorCode.INVALID_PRIVATE_KEY, e);
    }
  }
}
