/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.gui.controller;

import ch.ge.ve.chvote.pactback.contract.printerarchive.PrinterOperationConfigurationVo;
import ch.ge.ve.interfaces.ech.eCH0228.v1.VotingCardDeliveryType;
import ch.ge.ve.model.convert.api.MissingElectionIdentifierException;
import ch.ge.ve.model.convert.impl.ech0159.ECH0159Parser;
import ch.ge.ve.model.convert.impl.ech0159.model.ECH0159VoteInformation;
import ch.ge.ve.uprint.business.context.api.ContextService;
import ch.ge.ve.uprint.business.context.model.Context;
import ch.ge.ve.uprint.business.ech0228.VotingCardNavigator;
import ch.ge.ve.uprint.business.solr.api.SolrService;
import ch.ge.ve.uprint.gui.controller.model.AnswerModelObject;
import ch.ge.ve.uprint.gui.controller.model.QuestionModelObject;
import ch.ge.ve.uprint.gui.controller.model.VotingCardModelObject;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.Property;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyListProperty;
import javafx.beans.property.ReadOnlyLongProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TreeTableView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * JavaFX Controller for the voting cards visualization.
 */
@Component
public class VotingCardsController {

  private final Property<PrinterOperationConfigurationVo> operationConfigurationProperty;
  private final StringProperty                            currentPrinterFile;
  private final ListProperty<String>                      currentBallots;
  private final ObjectProperty<VotingCardModelObject>     currentVotingCardProperty;
  private final BooleanProperty                           hasNext;
  private final BooleanProperty                           hasPrevious;
  private final IntegerProperty                           currentVotingCardIndexProperty;
  private final LongProperty                              nbrOfVotingCardsProperty;

  private final SolrService    solrService;
  private final ContextService contextService;

  private VotingCardNavigator          votingCardNavigator;
  private List<ECH0159VoteInformation> voteInformationList;

  @FXML
  private HBox tableContainer;

  @FXML
  private JFXComboBox<String> ballotComboBox;

  @FXML
  private JFXComboBox<String> eCH0228ComboBox;

  @Autowired
  public VotingCardsController(SolrService solrService, ContextService contextService) {
    this.solrService = solrService;
    this.contextService = contextService;

    this.operationConfigurationProperty = new SimpleObjectProperty<>(this, "operationConfigurationProperty");
    this.currentPrinterFile = new SimpleStringProperty(this, "currentPrinterFile");
    this.currentBallots = new SimpleListProperty<>(this, "currentBallots");
    this.currentVotingCardProperty = new SimpleObjectProperty<>(this, "currentVotingCardProperty");
    this.hasNext = new SimpleBooleanProperty(this, "hasNext", false);
    this.hasPrevious = new SimpleBooleanProperty(this, "hasPrevious", false);
    this.currentVotingCardIndexProperty = new SimpleIntegerProperty(this, "currentVotingCardIndexProperty", 1);
    this.nbrOfVotingCardsProperty = new SimpleLongProperty(this, "nbrOfVotingCardsProperty", 0);
  }

  /**
   * Initialize the voting card navigator and display the first returned card.
   */
  @FXML
  public void initialize() {
    Context context = contextService.get();
    ECH0159Parser ech0159Parser = new ECH0159Parser();

    ballotComboBox.getSelectionModel().selectedItemProperty().addListener(
        (observable, oldValue, newValue) -> onBallotSelected(newValue)
    );

    votingCardNavigator = new VotingCardNavigator(solrService.getClient());
    nbrOfVotingCardsProperty.set(votingCardNavigator.size());
    currentPrinterFile.setValue(context.getPrinterFile().getSource().getFileName().toString());

    eCH0228ComboBox.setItems(FXCollections.observableArrayList(
        context.getPrinterFile().getEch0228FilePath().getFileName().toString()
    ));
    eCH0228ComboBox.getSelectionModel().select(0);

    operationConfigurationProperty.setValue(context.getPrinterFile().getOperationConfiguration());

    voteInformationList = context.getPrinterFile()
                                 .getOperationReferenceFiles()
                                 .stream()
                                 .map(opRefFile -> {
                                   try (InputStream in = Files.newInputStream(opRefFile)) {
                                     return ech0159Parser.retrieveVoteInformation(in);
                                   } catch (IOException | MissingElectionIdentifierException e) {
                                     throw new IllegalArgumentException("Cannot parse operation reference file", e);
                                   }
                                 })
                                 .flatMap(List::stream)
                                 .collect(Collectors.toList());

    next();
  }

  /**
   * Display the next voting card.
   */
  @FXML
  public void next() {
    setCurrentVotingCard(votingCardNavigator.next());
  }

  /**
   * Display the previous voting card.
   */
  @FXML
  public void previous() {
    setCurrentVotingCard(votingCardNavigator.previous());
  }

  /**
   * Display the first voting card.
   */
  @FXML
  public void first() {
    setCurrentVotingCard(votingCardNavigator.first());
  }

  /**
   * Display the last voting card.
   */
  @FXML
  public void last() {
    setCurrentVotingCard(votingCardNavigator.last());
  }

  private void onBallotSelected(String ballot) {
    tableContainer.getChildren().clear();
    if (ballot != null) {
      JFXTreeTableView<QuestionModelObject> codesTable = new JFXTreeTableView<>();

      List<QuestionModelObject> questions = currentVotingCardProperty.getValue().getVoteBallots().get(ballot);

      int nbrOfAnswerColumns = questions.stream()
                                        .map(QuestionModelObject::getAnswer)
                                        .mapToInt(List::size)
                                        .max().orElse(0);

      JFXTreeTableColumn<QuestionModelObject, String> idColumn = createColumn("id", nbrOfAnswerColumns + 1, codesTable);
      setCellValueFactory(idColumn, QuestionModelObject::getQuestionIdentification);
      codesTable.getColumns().add(idColumn);

      for (int i = 0; i < nbrOfAnswerColumns; i++) {
        final int columnIndex = i;
        JFXTreeTableColumn<QuestionModelObject, String> answerColumn = createColumn("Answer " + i,
                                                                                    nbrOfAnswerColumns + 1,
                                                                                    codesTable);

        setCellValueFactory(answerColumn, question -> {
          List<AnswerModelObject> answers = question.getAnswer();
          if (columnIndex < answers.size()) {
            AnswerModelObject currentAnswer = answers.get(columnIndex);
            return currentAnswer.getAnswer() + ": " + currentAnswer.getVerificationCode();
          } else {
            return "";
          }
        });

        codesTable.getColumns().add(answerColumn);
      }

      codesTable.setColumnResizePolicy(TreeTableView.CONSTRAINED_RESIZE_POLICY);
      codesTable.setRoot(new RecursiveTreeItem<>(FXCollections.observableArrayList(questions),
                                                 RecursiveTreeObject::getChildren));
      codesTable.setShowRoot(false);

      tableContainer.getChildren().add(codesTable);
      HBox.setHgrow(codesTable, Priority.ALWAYS);
    }
  }

  private JFXTreeTableColumn<QuestionModelObject, String> createColumn(String columnName,
                                                                       int columnSize,
                                                                       JFXTreeTableView<QuestionModelObject> table) {

    JFXTreeTableColumn<QuestionModelObject, String> column = new JFXTreeTableColumn<>(columnName);
    column.setResizable(false);
    column.prefWidthProperty().bind(table.widthProperty().subtract(25).divide(columnSize));
    return column;

  }

  private void setCellValueFactory(JFXTreeTableColumn<QuestionModelObject, String> column,
                                   Function<QuestionModelObject, String> mapper) {
    column.setCellValueFactory(
        param -> {
          if (column.validateValue(param)) {
            return new SimpleStringProperty(mapper.apply(param.getValue().getValue()));
          } else {
            return column.getComputedValue(param);
          }
        }
    );
  }

  private void setCurrentVotingCard(VotingCardDeliveryType.VotingCard votingCardType) {
    VotingCardModelObject votingCardModelObject = VotingCardModelObject.get(votingCardType, voteInformationList);

    currentVotingCardProperty.setValue(votingCardModelObject);

    ballotComboBox.setItems(FXCollections.observableArrayList(votingCardModelObject.getVoteBallots().keySet()));
    ballotComboBox.getSelectionModel().select(0);

    hasNext.setValue(votingCardNavigator.hasNext());
    hasPrevious.setValue(votingCardNavigator.hasPrevious());

    // Work around to avoid automatic focus on the next element
    if (!hasNext.get()) {
      tableContainer.requestFocus();
    }

    // Start index for the GUI is 1
    currentVotingCardIndexProperty.set(votingCardNavigator.currentIndex() + 1);
  }

  /*--------------------------------------------------------------------------
   - Properties                                                              -
   ---------------------------------------------------------------------------*/

  public PrinterOperationConfigurationVo getOperationConfiguration() {
    return operationConfigurationProperty.getValue();
  }

  public Property<PrinterOperationConfigurationVo> operationConfigurationProperty() {
    return operationConfigurationProperty;
  }

  public String getCurrentPrinterFile() {
    return currentPrinterFile.get();
  }

  public ReadOnlyStringProperty currentPrinterFileProperty() {
    return currentPrinterFile;
  }

  public VotingCardModelObject getCurrentVotingCard() {
    return currentVotingCardProperty.getValue();
  }

  public ReadOnlyObjectProperty<VotingCardModelObject> currentVotingCardProperty() {
    return currentVotingCardProperty;
  }

  public ObservableList<String> getCurrentBallots() {
    return currentBallots.getValue();
  }

  public ReadOnlyListProperty<String> currentBallotsProperty() {
    return currentBallots;
  }

  public boolean getHasPrevious() {
    return hasPrevious.get();
  }

  public ReadOnlyBooleanProperty hasPreviousProperty() {
    return hasPrevious;
  }

  public boolean getHasNext() {
    return hasNext.get();
  }

  public ReadOnlyBooleanProperty hasNextProperty() {
    return hasNext;
  }

  public int getCurrentVotingCardIndex() {
    return currentVotingCardIndexProperty.get();
  }

  public ReadOnlyIntegerProperty currentVotingCardIndexProperty() {
    return currentVotingCardIndexProperty;
  }

  public long getNbrOfVotingCards() {
    return nbrOfVotingCardsProperty.get();
  }

  public ReadOnlyLongProperty nbrOfVotingCardsProperty() {
    return nbrOfVotingCardsProperty;
  }
}
