/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.gui.controller.format

import ch.ge.ve.interfaces.ech.eCH0010.v6.PersonMailAddressInfoType
import ch.ge.ve.javafx.business.i18n.LanguageUtils
import spock.lang.Specification

class PersonInfoFormatterTest extends Specification {

  private static final Locale initialLocale = Locale.getDefault() // so that we can reset any change during the tests

  def formatter = new PersonInfoFormatter()

  void cleanup() {
    Locale.setDefault(initialLocale)
  }

  def "AsString should dispay all needed info"() {
    given:
    def person = new PersonMailAddressInfoType(mrMrs: "1", lastName: "Doe", firstName: "Jane", title: "PhD")
    LanguageUtils.setCurrentLocale(Locale.FRENCH)

    expect:
    formatter.asString(person) == "Madame DOE Jane"
  }

  def "AsString should adapt the civility to the current language"(String echCivility, Locale locale, String expected) {
    expect:
    LanguageUtils.setCurrentLocale(locale)
    def person = new PersonMailAddressInfoType(mrMrs: echCivility, lastName: "Example", firstName: "Blah")
    formatter.asString(person).startsWith("$expected ")

    where:
    echCivility | locale         || expected
    "1"         | Locale.FRENCH  || "Madame"
    "1"         | Locale.GERMAN  || "DE - Madame"
    "1"         | Locale.ENGLISH || "Madame"
    "2"         | Locale.FRENCH  || "Monsieur"
    "2"         | Locale.GERMAN  || "DE - Monsieur"
  }

  def "AsString should display an error indication for any undefined echValue"() {
    given:
    def undefinedValue = "0"
    def person = new PersonMailAddressInfoType(mrMrs: undefinedValue, lastName: "Doe", firstName: "Snoopy")

    expect:
    formatter.asString(person) == "?? mrMrs=0 ?? DOE Snoopy"
  }

  def "AsString should filter every unset value"(String civility, String lastName, String firstName, String expected) {
    given:
    LanguageUtils.setCurrentLocale(Locale.FRENCH)

    expect:
    def person = new PersonMailAddressInfoType(mrMrs: civility, lastName: lastName, firstName: firstName)
    formatter.asString(person) == expected

    where:
    civility | lastName | firstName || expected
    null     | "Doe"    | "John"    || "DOE John"
    "2"      | null     | "John"    || "Monsieur John"
    "2"      | "Doe"    | null      || "Monsieur DOE"
    ""       | ""       | ""        || ""
  }

}
