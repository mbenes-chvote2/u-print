/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.solr;

import ch.ge.ve.javafx.business.progress.ProgressTracker;
import ch.ge.ve.uprint.business.solr.api.UprintSolrCore;
import ch.ge.ve.uprint.business.solr.model.ImportReport;
import org.apache.solr.client.solrj.SolrClient;

/**
 * A template class for importing solr documents.
 *
 * @param <T> The type of document to import.
 */
public abstract class DocumentImporter<T> {
  private final SolrBatchInserter<T> solrBatchInserter;
  private final ProgressTracker      progressTracker;

  /**
   * Create a new solr document importer.
   *
   * @param solrClient      the solr client.
   * @param core            the core where this instance will import documents.
   * @param progressTracker a progress tracker strategy.
   */
  public DocumentImporter(SolrClient solrClient, UprintSolrCore core, ProgressTracker progressTracker) {
    this.solrBatchInserter = new SolrBatchInserter<>(solrClient, core);
    this.progressTracker = progressTracker;
  }

  /**
   * Classes implementing this template should call this function in order to store new documents to solr.
   *
   * @param next  the next document to store.
   * @param done  the amount of done units.
   * @param total the total number of units to do.
   */
  protected void next(T next, long done, long total) {
    solrBatchInserter.add(next);
    progressTracker.updateProgress(done, total);
  }

  /**
   * Classes implementing this template should call this function if the import has been aborted.
   */
  protected void abort() {
    solrBatchInserter.flush();
  }

  /**
   * Classes implementing this template should call this function once they've finished importing all the documents.
   *
   * @return the number of imported documents.
   */
  protected long done() {
    solrBatchInserter.flush();
    progressTracker.updateProgress(1, 1);

    return solrBatchInserter.getNbrOfDocumentsImported();
  }

  /**
   * Perform the import task.
   *
   * @return a {@link ImportReport} instance with a summary of what was imported.
   */
  public abstract ImportReport doImport();
}
