/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.ech0045;

import ch.ge.ve.interfaces.ech.eCH0045.v4.VotingPersonType;
import ch.ge.ve.javafx.business.progress.ProgressTracker;
import ch.ge.ve.uprint.business.ech0045.exception.ECH0045ParsingException;
import ch.ge.ve.uprint.business.solr.DocumentImporter;
import ch.ge.ve.uprint.business.solr.api.UprintSolrCore;
import ch.ge.ve.uprint.business.solr.model.ImportReport;
import ch.ge.ve.uprint.business.solr.model.VoterDocument;
import ch.ge.ve.uprint.business.xml.ECHUtils;
import com.google.common.base.Stopwatch;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import org.apache.solr.client.solrj.SolrClient;
import org.codehaus.stax2.XMLInputFactory2;
import org.codehaus.stax2.XMLOutputFactory2;

/**
 * An object to parse an electoral register (eCH-0045) and to store the voters into the Solr index.
 */
public class ECH0045Parser extends DocumentImporter<VoterDocument> {
  private static final String VOTER = "voter";

  private final Path        source;
  private final JAXBContext jaxbContext;

  private final XMLOutputFactory xmlOutputFactory = XMLOutputFactory2.newFactory();
  private final XMLInputFactory  xmlInputFactory  = XMLInputFactory2.newInstance();
  private final XMLEventFactory  eventFactory     = XMLEventFactory.newInstance();

  /**
   * Create a new eCH-0045 parser instance.
   *
   * @param solrClient      the solr client.
   * @param progressTracker a progress tracking strategy.
   * @param source          the ecH-0045 XML file location.
   */
  public ECH0045Parser(SolrClient solrClient,
                       ProgressTracker progressTracker,
                       Path source) {
    super(solrClient, UprintSolrCore.ELECTORAL_REGISTER, progressTracker);
    this.source = source;

    try {
      this.jaxbContext = JAXBContext.newInstance(VotingPersonType.class);
    } catch (JAXBException e) {
      throw new ECH0045ParsingException("Cannot initialize ECH0045 parser", e);
    }
  }

  @Override
  public ImportReport doImport() {
    Stopwatch stopwatch = Stopwatch.createStarted();

    try (InputStream in = new FileInputStream(source.toFile())) {
      long sizeInBytes = Files.size(source);

      XMLStreamReader streamReader = xmlInputFactory.createXMLStreamReader(in);
      XMLEventReader eventReader = xmlInputFactory.createXMLEventReader(streamReader);


      while (eventReader.hasNext()) {
        long bytesRead = streamReader.getLocation().getCharacterOffset();
        XMLEvent event = eventReader.peek();

        if (event.isStartElement() && VOTER.equals(event.asStartElement().getName().getLocalPart())) {
          this.next(toVoterDocument(eventReader), bytesRead, sizeInBytes);
        } else {
          eventReader.next();
        }
      }
    } catch (JAXBException | IOException | XMLStreamException | IllegalArgumentException e) {
      abort();
      throw new ECH0045ParsingException(String.format("Error caught while parsing file [%s]", source), e);
    }

    return new ImportReport(done(), stopwatch.stop().elapsed());
  }

  private VoterDocument toVoterDocument(XMLEventReader eventReader)
      throws XMLStreamException, IOException, JAXBException {
    Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
    Marshaller marshaller = jaxbContext.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

    ByteArrayOutputStream baos = new ByteArrayOutputStream();

    XMLEventWriter writer = xmlOutputFactory.createXMLEventWriter(baos, StandardCharsets.UTF_8.name());
    writer.add(eventFactory.createStartDocument(StandardCharsets.UTF_8.name(), "1.0"));
    ECHUtils.setCommonPrefixes(writer);
    writer.add(eventFactory.createStartElement("", null, "root"));
    ECHUtils.writeCommonNamespaces(writer, eventFactory);

    JAXBElement<VotingPersonType> element = unmarshaller.unmarshal(eventReader, VotingPersonType.class);
    VotingPersonType votingPerson = element.getValue();
    marshaller.marshal(element, writer);

    writer.add(eventFactory.createEndElement("", null, "root"));
    writer.add(eventFactory.createEndDocument());
    writer.flush();
    writer.close();

    return new VoterDocument(ECH0045Utils.retrievePersonId(votingPerson), baos.toString(StandardCharsets.UTF_8.name()));
  }
}
