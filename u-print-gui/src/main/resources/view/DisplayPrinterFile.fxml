<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ #%L
  ~ u-print
  ~ %%
  ~ Copyright (C) 2016 - 2018 République et Canton de Genève
  ~ %%
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  ~ #L%
  -->

<?import ch.ge.ve.javafx.gui.control.MessageContainer?>
<?import com.jfoenix.controls.JFXButton?>
<?import com.jfoenix.controls.JFXProgressBar?>
<?import com.jfoenix.controls.JFXTextField?>
<?import com.jfoenix.validation.RequiredFieldValidator?>
<?import de.jensd.fx.glyphs.materialicons.MaterialIconView?>
<?import javafx.scene.control.Label?>
<?import javafx.scene.layout.HBox?>
<?import javafx.scene.layout.StackPane?>
<?import javafx.scene.layout.VBox?>
<StackPane xmlns="http://javafx.com/javafx" xmlns:fx="http://javafx.com/fxml"
           styleClass="card"
           fx:controller="ch.ge.ve.uprint.gui.controller.DisplayPrinterFileController">
    <VBox alignment="CENTER">
        <HBox styleClass="card-header">
            <Label text="%display-printer-file.title" styleClass="card-title"/>
        </HBox>

        <MessageContainer fx:id="errorMessageContainer" alignment="CENTER_LEFT" messageType="ERROR"/>

        <VBox alignment="CENTER" styleClass="card-content" spacing="20"
              managed="${!controller.printerFileImportInProgress}"
              visible="${!controller.printerFileImportInProgress}">
            <VBox spacing="10">
                <Label styleClass="h4,mandatory" text="%display-printer-file.add-printer-file"/>
                <HBox alignment="CENTER_LEFT" spacing="5">
                    <JFXButton onMousePressed="#selectPrinterFile" text="%display-printer-file.browse"
                               buttonType="FLAT" styleClass="button-primary">
                        <graphic>
                            <MaterialIconView glyphName="ATTACH_FILE" styleClass="icon-primary"/>
                        </graphic>
                    </JFXButton>
                    <JFXTextField editable="false" fx:id="printerFilePath" HBox.hgrow="ALWAYS">
                        <validators>
                            <RequiredFieldValidator message="%error.no-printer-file"/>
                        </validators>
                    </JFXTextField>
                </HBox>
            </VBox>
        </VBox>

        <HBox alignment="BOTTOM_RIGHT" styleClass="card-actions"
              managed="${!controller.printerFileImportInProgress}"
              visible="${!controller.printerFileImportInProgress}">
            <JFXButton styleClass="button-primary-raised" text="%display-printer-file.submit" buttonType="RAISED"
                       onMousePressed="#doImport"/>
        </HBox>

        <VBox alignment="CENTER"
              managed="${controller.printerFileImportInProgress}"
              visible="${controller.printerFileImportInProgress}">
            <HBox alignment="CENTER_LEFT" fx:id="extractPrinterFileProgressRow" styleClass="progress-row">
                <Label alignment="CENTER_RIGHT" text="%display-printer-file.extract-printer-file-progress"
                       styleClass="progress-label"/>
                <JFXProgressBar fx:id="extractPrinterFileProgress"/>
            </HBox>
            <HBox alignment="CENTER_LEFT" fx:id="importPrinterFileProgressRow" styleClass="progress-row">
                <Label alignment="CENTER_RIGHT" text="%display-printer-file.import-printer-file-progress"
                       styleClass="progress-label"/>
                <JFXProgressBar fx:id="importPrinterFileProgress"/>
            </HBox>
        </VBox>

        <VBox fx:id="votingCardBox" alignment="CENTER" styleClass="voting-card-container"
              visible="false" managed="false" spacing="20"/>
    </VBox>
</StackPane>
