/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.key;

import ch.ge.ve.crypto.exception.KeyStoreNotEmptyException;
import ch.ge.ve.crypto.impl.PrivateKeyStorePkcs12Impl;
import ch.ge.ve.javafx.business.json.JsonPathMapper;
import ch.ge.ve.protocol.core.algorithm.KeyEstablishmentAlgorithms;
import ch.ge.ve.protocol.core.model.IdentificationKeyPair;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.protocol.model.IdentificationGroup;
import ch.ge.ve.protocol.model.IdentificationPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.uprint.business.key.exception.PrinterKeyGenerationException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * A service to generate and store a key pair.
 */
@Service
public class PrinterKeyGenerator {
  private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH'h'mm'm'ss's'");

  private final JsonPathMapper             jsonPathMapper;
  private final IdentificationGroup        identificationGroup;
  private final KeyEstablishmentAlgorithms keyEstablishmentAlgorithms;

  /**
   * Create a new printer key generator.
   *
   * @param jsonPathMapper             the json mapper.
   * @param publicParameters           the default public parameters.
   * @param keyEstablishmentAlgorithms the key generation algorithm.
   */
  @Autowired
  public PrinterKeyGenerator(JsonPathMapper jsonPathMapper,
                             PublicParameters publicParameters,
                             KeyEstablishmentAlgorithms keyEstablishmentAlgorithms) {
    this.jsonPathMapper = jsonPathMapper;
    this.identificationGroup = publicParameters.getIdentificationGroup();
    this.keyEstablishmentAlgorithms = keyEstablishmentAlgorithms;
  }

  /**
   * Generate a pair of keys and stores in the given folder. The private part will be safely stored using the provided
   * passphrase in a file named: <code>private-key.pfx</code> and the public will be available in a file named:
   * <code>public-key.json</code>.
   *
   * @param outputFolder the folder where the pair of keys will be stored.
   * @param passphrase   the passphrase to protect the private key.
   */
  public void generateAndStoreKeyPair(Path outputFolder, String passphrase) {
    IdentificationKeyPair keyPair = keyEstablishmentAlgorithms.generateKeyPair(identificationGroup);

    storePrivateKey(
        keyPair.getPrivateKey(),
        passphrase,
        outputFolder.resolve(buildFileName("private-key", "pfx"))
    );

    storePublicKey(
        keyPair.getPublicKey(),
        outputFolder.resolve(buildFileName("public-key", "json"))
    );

  }

  private void storePublicKey(IdentificationPublicKey publicKey, Path outputFile) {
    jsonPathMapper.write(outputFile, publicKey);
  }

  private void storePrivateKey(IdentificationPrivateKey privateKey, String passphrase, Path outputFile) {
    try {
      new PrivateKeyStorePkcs12Impl(outputFile)
          .storeKey(privateKey.getPrivateKey().toByteArray(), passphrase.toCharArray());
    } catch (KeyStoreNotEmptyException e) {
      throw new PrinterKeyGenerationException("An error occurred when storing the private key", e);
    }
  }

  private String buildFileName(String prefix, String suffix) {
    return String.format("%s_%s.%s", prefix, DATE_FORMAT.format(LocalDateTime.now()), suffix);
  }
}
