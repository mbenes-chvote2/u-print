/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.gui.controller.model;

import ch.ge.ve.interfaces.ech.eCH0228.v1.VoteType;
import ch.ge.ve.javafx.business.i18n.LanguageUtils;
import ch.ge.ve.model.convert.model.AnswerTypeEnum;

/**
 * A {@link AnswerModelObject} value object. New instances of this value object can only be created with the provided
 * factory method: {@link AnswerModelObject#get(VoteType.Ballot.VariantBallot.Question.AnswerOption, AnswerTypeEnum)}.
 */
public class AnswerModelObject {
  private final String answer;
  private final String verificationCode;

  /**
   * Create a new answer instance based on the given
   * {@link VoteType.Ballot.VariantBallot.Question.AnswerOption}.
   *
   * @param answer        the answer object extracted from an eCH-0228 XML file.
   * @param answerTypeRef the {@link AnswerTypeEnum} mapping to this answer.
   *
   * @return the new instance.
   */
  public static AnswerModelObject get(VoteType.Ballot.VariantBallot.Question.AnswerOption answer,
                                      AnswerTypeEnum answerTypeRef) {
    return new AnswerModelObject(
        LanguageUtils.getCurrentResourceBundle().getString(
            String.format("voting-card.ballots.answer.%s",
                          answerTypeRef.getOptions().get(
                              answer.getAnswerSequenceNumber().intValue()
                          )
            )),
        answer.getIndividualAnswerVerificationCode()
    );
  }

  /**
   * Create a new instance based on the given
   * {@link VoteType.Ballot.VariantBallot.Question.AnswerOption}.
   *
   * @param answer        the answer object extracted from an eCH-0228 XML file.
   * @param answerTypeRef the {@link AnswerTypeEnum} mapping to this answer.
   *
   * @return the new instance.
   */
  public static AnswerModelObject get(VoteType.Ballot.StandardBallot.Question.AnswerOption answer,
                                      AnswerTypeEnum answerTypeRef) {
    return new AnswerModelObject(
        LanguageUtils.getCurrentResourceBundle().getString(
            String.format("voting-card.ballots.answer.%s",
                          answerTypeRef.getOptions().get(
                              answer.getAnswerSequenceNumber().intValue()
                          )
            )),
        answer.getIndividualAnswerVerificationCode()
    );
  }

  private AnswerModelObject(String answer, String verificationCode) {
    this.answer = answer;
    this.verificationCode = verificationCode;
  }

  public String getAnswer() {
    return answer;
  }

  public String getVerificationCode() {
    return verificationCode;
  }
}
