/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.gui.controller.model;

import ch.ge.ve.interfaces.ech.eCH0228.v1.VoteType;
import ch.ge.ve.model.convert.model.AnswerTypeEnum;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A {@link QuestionModelObject} value object. New instances of this value object can only be created with the provided
 * factory methods: {@link QuestionModelObject#get(VoteType.Ballot.StandardBallot.Question, AnswerTypeEnum)},
 * {@link QuestionModelObject#get(VoteType.Ballot.VariantBallot.Question, AnswerTypeEnum)}.
 */
public class QuestionModelObject extends RecursiveTreeObject<QuestionModelObject> {
  private final String                  questionIdentification;
  private final List<AnswerModelObject> answer;

  /**
   * Create a new {@link QuestionModelObject} instance based on the given
   * {@link VoteType.Ballot.StandardBallot.Question}.
   *
   * @param question      the question object extracted from an eCH-0228 XML file.
   * @param answerTypeRef the {@link AnswerTypeEnum} mapping to this question's answer type.
   *
   * @return the new {@link QuestionModelObject} instance.
   */
  public static QuestionModelObject get(VoteType.Ballot.StandardBallot.Question question,
                                        AnswerTypeEnum answerTypeRef) {
    return new QuestionModelObject(question.getQuestionIdentification(),
                                   question.getAnswerOption()
                                           .stream()
                                           .map(answerOption -> AnswerModelObject.get(answerOption, answerTypeRef))
                                           .collect(Collectors.toList()));
  }

  /**
   * Create a new {@link QuestionModelObject} instance based on the given
   * {@link VoteType.Ballot.VariantBallot.Question}.
   *
   * @param question      the question object extracted from an eCH-0228 XML file.
   * @param answerTypeRef the {@link AnswerTypeEnum} mapping to this question's answer type.
   *
   * @return the new {@link QuestionModelObject} instance.
   */
  public static QuestionModelObject get(VoteType.Ballot.VariantBallot.Question question,
                                        AnswerTypeEnum answerTypeRef) {
    return new QuestionModelObject(question.getQuestionIdentification(),
                                   question.getAnswerOption()
                                           .stream()
                                           .map(answerOption -> AnswerModelObject.get(answerOption, answerTypeRef))
                                           .collect(Collectors.toList()));
  }

  private QuestionModelObject(String questionIdentification, List<AnswerModelObject> answer) {
    this.questionIdentification = questionIdentification;
    this.answer = answer;
  }

  public String getQuestionIdentification() {
    return questionIdentification;
  }

  public List<AnswerModelObject> getAnswer() {
    return answer;
  }
}
