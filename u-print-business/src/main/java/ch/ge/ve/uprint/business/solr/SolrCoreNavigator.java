/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.solr;

import ch.ge.ve.uprint.business.solr.api.UprintSolrCore;
import ch.ge.ve.uprint.business.solr.exception.SolrRuntimeException;
import ch.ge.ve.uprint.business.util.Navigator;
import java.io.IOException;
import java.util.NoSuchElementException;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;

/**
 * Navigate over all the documents returned by a given solr query.
 *
 * @param <T> the type of document.
 */
public class SolrCoreNavigator<T> implements Navigator<T> {
  private final SolrClient     solrClient;
  private final UprintSolrCore core;
  private final SolrQuery      query;
  private final Class<T>       type;

  private int currentPosition;

  /**
   * Create a new solr core navigator.
   *
   * @param solrClient the {@link SolrClient} that will execute the query.
   * @param core       the core where the documents will be requested.
   * @param query      the query to navigate.
   * @param type       the type of document to return.
   */
  public SolrCoreNavigator(SolrClient solrClient, UprintSolrCore core, SolrQuery query, Class<T> type) {
    this.solrClient = solrClient;
    this.core = core;
    this.query = query;
    this.type = type;
    this.currentPosition = -1;
  }

  private T get(int index) {
    try {
      query.setStart(index);
      query.setRows(1);
      return solrClient.query(core.getCoreName(), query).getBeans(type).stream().findFirst().orElse(null);
    } catch (SolrServerException | IOException e) {
      throw new SolrRuntimeException("", e);
    }
  }

  @Override
  public boolean hasNext() {
    return get(currentPosition + 1) != null;
  }

  @Override
  public T next() {
    T result = get(currentPosition + 1);
    if (result == null) {
      throw new NoSuchElementException();
    }
    currentPosition += 1;
    return result;
  }

  @Override
  public boolean hasPrevious() {
    return currentPosition > 0;
  }

  @Override
  public T previous() {
    if (!hasPrevious()) {
      throw new NoSuchElementException();
    }
    T result = get(currentPosition - 1);
    currentPosition -= 1;
    return result;
  }

  @Override
  public int currentIndex() {
    return currentPosition;
  }

  @Override
  public T last() {
    int lastPosition = size().intValue() - 1;
    T result = get(lastPosition);
    if (result == null) {
      throw new NoSuchElementException();
    }
    currentPosition = lastPosition;
    return result;
  }

  @Override
  public T first() {
    T result = get(0);
    if (result == null) {
      throw new NoSuchElementException();
    }
    currentPosition = 0;
    return result;
  }

  @Override
  public Long size() {
    query.setRows(0);

    try {
      return solrClient.query(core.getCoreName(), query).getResults().getNumFound();
    } catch (SolrServerException | IOException e) {
      throw new SolrRuntimeException(
          String.format("Failed to retrieve number of documents on [%s] index", core.getCoreName()), e);
    }
  }
}
