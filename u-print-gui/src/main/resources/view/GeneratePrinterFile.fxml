<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ #%L
  ~ u-print
  ~ %%
  ~ Copyright (C) 2016 - 2018 République et Canton de Genève
  ~ %%
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  ~ #L%
  -->

<?import ch.ge.ve.javafx.gui.control.MessageContainer?>
<?import ch.ge.ve.uprint.gui.validator.PrivateKeyValidator?>
<?import com.jfoenix.controls.JFXButton?>
<?import com.jfoenix.controls.JFXDialog?>
<?import com.jfoenix.controls.JFXDialogLayout?>
<?import com.jfoenix.controls.JFXPasswordField?>
<?import com.jfoenix.controls.JFXProgressBar?>
<?import com.jfoenix.controls.JFXTextField?>
<?import com.jfoenix.validation.RequiredFieldValidator?>
<?import de.jensd.fx.glyphs.materialicons.MaterialIconView?>
<?import javafx.scene.control.Label?>
<?import javafx.scene.layout.HBox?>
<?import javafx.scene.layout.StackPane?>
<?import javafx.scene.layout.VBox?>
<StackPane xmlns="http://javafx.com/javafx" xmlns:fx="http://javafx.com/fxml"
           styleClass="card" fx:id="root"
           fx:controller="ch.ge.ve.uprint.gui.controller.GeneratePrinterFileController">
    <VBox alignment="CENTER">
        <HBox styleClass="card-header">
            <Label text="%generate-printer-file.title" styleClass="card-title"/>
        </HBox>

        <MessageContainer fx:id="errorMessageContainer" alignment="CENTER_LEFT" messageType="ERROR"/>

        <VBox alignment="CENTER" styleClass="card-content" fx:id="importBox" spacing="20">
            <VBox spacing="10">
                <Label styleClass="h4,mandatory" text="%generate-printer-file.add-printer-archive"/>
                <HBox alignment="CENTER_LEFT" spacing="5">
                    <JFXButton onMousePressed="#selectPrinterArchiveFile" text="%generate-printer-file.browse"
                               buttonType="FLAT" styleClass="button-primary">
                        <graphic>
                            <MaterialIconView glyphName="ATTACH_FILE" styleClass="icon-primary"/>
                        </graphic>
                    </JFXButton>
                    <JFXTextField editable="false" fx:id="printerArchivePath" HBox.hgrow="ALWAYS">
                        <validators>
                            <RequiredFieldValidator message="%error.no-printer-archive"/>
                        </validators>
                    </JFXTextField>
                </HBox>
            </VBox>
            <VBox spacing="10">
                <Label styleClass="h4,mandatory" text="%generate-printer-file.add-private-key"/>
                <HBox alignment="CENTER_LEFT" spacing="5">
                    <JFXButton onMousePressed="#selectPrivateKeyFile" text="%generate-printer-file.browse"
                               buttonType="FLAT"
                               styleClass="button-primary">
                        <graphic>
                            <MaterialIconView glyphName="ATTACH_FILE" styleClass="icon-primary"/>
                        </graphic>
                    </JFXButton>
                    <JFXTextField editable="false" fx:id="privateKeyPath" HBox.hgrow="ALWAYS">
                        <validators>
                            <RequiredFieldValidator message="%error.no-private-key"/>
                        </validators>
                    </JFXTextField>
                </HBox>
            </VBox>
            <VBox spacing="10">
                <Label styleClass="h4,mandatory" text="%generate-printer-file.add-destination-folder"/>
                <HBox alignment="CENTER_LEFT" spacing="5">
                    <JFXButton onMousePressed="#selectDestinationFolder" text="%generate-printer-file.browse"
                               buttonType="FLAT"
                               styleClass="button-primary">
                        <graphic>
                            <MaterialIconView glyphName="ATTACH_FILE" styleClass="icon-primary"/>
                        </graphic>
                    </JFXButton>
                    <JFXTextField editable="false" fx:id="destinationPath" HBox.hgrow="ALWAYS">
                        <validators>
                            <RequiredFieldValidator message="%error.no-destination-folder"/>
                        </validators>
                    </JFXTextField>
                </HBox>
            </VBox>
        </VBox>

        <HBox fx:id="actionBox" alignment="BOTTOM_RIGHT" styleClass="card-actions">
            <JFXButton styleClass="button-primary-raised" text="%generate-printer-file.submit" buttonType="RAISED"
                       onMousePressed="#showPassphraseDialog"/>
        </HBox>

        <VBox alignment="CENTER" fx:id="progressBox" visible="false" managed="false">
            <HBox alignment="CENTER_LEFT" fx:id="extractPrinterArchiveProgressRow" styleClass="progress-row">
                <Label alignment="CENTER_RIGHT" text="%generate-printer-file.extract-printer-archive-progress"
                       styleClass="progress-label"/>
                <JFXProgressBar fx:id="extractPrinterArchiveProgress"/>
            </HBox>
            <HBox alignment="CENTER_LEFT" fx:id="privateCredentialsProgressRow" styleClass="progress-row">
                <Label alignment="CENTER_RIGHT" text="%generate-printer-file.private-credentials-progress"
                       styleClass="progress-label"/>
                <JFXProgressBar fx:id="privateCredentialsProgress"/>
            </HBox>
            <VBox styleClass="progress-row">
                <HBox alignment="CENTER_LEFT" fx:id="votersRegisterProgressRow">
                    <Label alignment="CENTER_RIGHT" text="%generate-printer-file.printer-file-progress"
                           styleClass="progress-label"/>
                    <JFXProgressBar fx:id="votersRegisterProgress"/>
                </HBox>
                <HBox alignment="CENTER_LEFT" fx:id="votersRegisterResultsRow" managed="false" visible="false">
                    <Label styleClass="progress-label"/>
                    <Label text="%generate-printer-file.nbr-of-voters" styleClass="progress-info"/>
                </HBox>
            </VBox>
            <HBox alignment="CENTER_LEFT" fx:id="printerFileGenerationProgressRow" styleClass="progress-row">
                <Label alignment="CENTER_RIGHT" text="%generate-printer-file.printer-file-generation-progress"
                       styleClass="progress-label"/>
                <JFXProgressBar fx:id="printerFileGenerationProgress"/>
            </HBox>
        </VBox>
    </VBox>

    <JFXDialog fx:id="dialog">
        <JFXDialogLayout>
            <heading>
                <Label text="%passphrase-dialog.title"/>
            </heading>
            <body>
                <VBox spacing="10">
                    <Label styleClass="h4,mandatory" text="%passphrase-dialog.input-passphrase"/>
                    <HBox alignment="CENTER_LEFT" spacing="5">
                        <JFXPasswordField fx:id="privateKeyPassphrase" HBox.hgrow="ALWAYS">
                            <validators>
                                <RequiredFieldValidator message="%error.no-passphrase"/>
                                <PrivateKeyValidator message="%error.unreadable-private-key"
                                                     privateKeyPath="${privateKeyPath.text}"/>
                            </validators>
                        </JFXPasswordField>
                    </HBox>
                </VBox>
            </body>
            <actions>
                <JFXButton onMousePressed="#cancelGeneration" text="%passphrase-dialog.cancel"/>
                <JFXButton onMousePressed="#doGeneration" text="%passphrase-dialog.submit" styleClass="button-primary"/>
            </actions>
        </JFXDialogLayout>
    </JFXDialog>

</StackPane>
