/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.key.exception;

import ch.ge.ve.javafx.business.exception.ErrorCodeBasedException;

/**
 * Thrown when an error occurred during the generation of a key pair.
 */
public class PrinterKeyGenerationException extends ErrorCodeBasedException {
  private static final String ERROR_CODE_SUFFIX = "PRINTER_KEY_GENERATION_ERROR";

  /**
   * Create a new printer key generation exception.
   *
   * @param message the detail message.
   * @param cause   the cause of this exception.
   *
   * @see RuntimeException#RuntimeException(String, Throwable)
   */
  public PrinterKeyGenerationException(String message, Exception cause) {
    super(ERROR_CODE_SUFFIX, message, cause);
  }

}
