/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.solr.model;

import java.time.Duration;

/**
 * An object representing the result of an import operation.
 */
public class ImportReport {
  private final Long     nbrOfDocuments;
  private final Duration elapsedTime;

  /**
   * Create a new import report.
   *
   * @param nbrOfDocuments the number of imported documents.
   * @param elapsedTime    the time it took to import the documents in nanoseconds.
   */
  public ImportReport(Long nbrOfDocuments, Duration elapsedTime) {
    this.nbrOfDocuments = nbrOfDocuments;
    this.elapsedTime = elapsedTime;
  }

  /**
   * Get the total number of imported documents.
   *
   * @return the number of imported documents.
   */
  public Long getNbrOfDocuments() {
    return nbrOfDocuments;
  }

  /**
   * Get the elapsed time of the import task.
   *
   * @return the elapsed time of the import tasks in nanoseconds.
   */
  public Duration getElapsedTime() {
    return elapsedTime;
  }
}
