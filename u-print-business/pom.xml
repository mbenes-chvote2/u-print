<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ #%L
  ~ u-print
  ~ %%
  ~ Copyright (C) 2016 - 2018 République et Canton de Genève
  ~ %%
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  ~ #L%
  -->

<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>ch.ge.ve.uprint</groupId>
        <artifactId>u-print-parent</artifactId>
        <version>0.0.1</version>
    </parent>

    <artifactId>u-print-business</artifactId>

    <name>U-Print Business</name>
    <description>Business layer for the U-Print application</description>
    <organization>
        <name>OCSIN-SIDP</name>
    </organization>

    <dependencies>
        <!-- CHVote dependencies -->
        <dependency>
            <groupId>ch.ge.ve.ui</groupId>
            <artifactId>chvote-fx-common</artifactId>
            <exclusions>
                <!-- Not necessary, but this makes it explicit that this module should not use GUI components -->
                <exclusion>
                    <groupId>com.jfoenix</groupId>
                    <artifactId>jfoenix</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>de.jensd</groupId>
                    <artifactId>fontawesomefx-materialdesignfont</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>de.jensd</groupId>
                    <artifactId>fontawesomefx-materialicons</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>ch.ge.ve</groupId>
            <artifactId>crypto-api</artifactId>
        </dependency>
        <dependency>
            <groupId>ch.ge.ve</groupId>
            <artifactId>crypto-impl</artifactId>
        </dependency>
        <dependency>
            <groupId>ch.ge.ve</groupId>
            <artifactId>protocol-algorithms</artifactId>
        </dependency>
        <dependency>
            <groupId>ch.ge.ve</groupId>
            <artifactId>chvote-protocol-model</artifactId>
        </dependency>
        <dependency>
            <groupId>ch.ge.ve</groupId>
            <artifactId>pact-back-contract</artifactId>
        </dependency>
        <dependency>
            <groupId>ch.ge.ve.interfaces</groupId>
            <artifactId>chvote-model-converter-api</artifactId>
        </dependency>
        <dependency>
            <groupId>ch.ge.ve.interfaces</groupId>
            <artifactId>chvote-interfaces-jaxb</artifactId>
        </dependency>
        <dependency>
            <groupId>ch.ge.ve.interfaces</groupId>
            <artifactId>file-namer</artifactId>
        </dependency>

        <!-- Additional dependencies -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-core</artifactId>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
        </dependency>
        <dependency>
            <groupId>javax.xml.bind</groupId>
            <artifactId>jaxb-api</artifactId>
        </dependency>
        <dependency>
            <groupId>javax.activation</groupId>
            <artifactId>javax.activation-api</artifactId>
        </dependency>
        <dependency>
            <groupId>org.codehaus.woodstox</groupId>
            <artifactId>stax2-api</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.solr</groupId>
            <artifactId>solr-solrj</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.solr</groupId>
            <artifactId>solr-core</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>org.apache.logging.log4j</groupId>
                    <artifactId>log4j-slf4j-impl</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.apache.solr</groupId>
            <artifactId>solr-dataimporthandler</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>org.apache.logging.log4j</groupId>
                    <artifactId>log4j-slf4j-impl</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <!-- Test dependencies -->
        <dependency>
            <groupId>ch.ge.ve</groupId>
            <artifactId>protocol-support</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>ch.ge.ve.interfaces</groupId>
            <artifactId>chvote-model-converter-impl</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>ch.ge.ve</groupId>
            <artifactId>jackson-serializer</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.bouncycastle</groupId>
            <artifactId>bcprov-jdk15on</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.glassfish.jaxb</groupId>
            <artifactId>jaxb-runtime</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.codehaus.woodstox</groupId>
            <artifactId>woodstox-core-asl</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.spockframework</groupId>
            <artifactId>spock-spring</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>
</project>